package common

type SkillID uint16

const (
	SKILL_COOKING SkillID = iota
	SKILL_HEALING
	SKILL_FISHING
	SHILL_MINING
	SKILL_CRAFTING
)

type Params struct {
	HP uint16
	EP uint16

	STR uint16
	DEX uint16
	WIS uint16
}

func (params *Params) MaxHP() uint16 {
	return params.STR
}

func (params *Params) MaxEP() uint16 {
	return (params.WIS + params.DEX) / 2
}
