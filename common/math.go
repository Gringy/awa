package common

import (
	"errors"
	"log"
	"math/rand"
)

type Point struct {
	X, Y int
}

func Sign(n int) int {
	switch {
	case n > 0:
		return 1
	case n < 0:
		return -1
	default:
		return 0
	}
}

func Abs(n int) int {
	if n >= 0 {
		return n
	} else {
		return -n
	}
}

type Dir int

const (
	DIR_UP Dir = iota
	DIR_DOWN
	DIR_LEFT
	DIR_RIGHT
)

func RandDir() Dir {
	return Dir(rand.Intn(4))
}

func RandInt(min, max int) int {
	return rand.Intn(max-min) + min
}

func NormalizeCoords(x, y int) (newX, newY, chunkX, chunkY int, err error) {
	switch {
	case x >= MAPDIM:
		chunkX = x / MAPDIM
		x = x % MAPDIM
	case x < 0:
		chunkX = x/MAPDIM - 1
		x = MAPDIM + x%MAPDIM
	}
	switch {
	case y >= MAPDIM:
		chunkY = y / MAPDIM
		y = y % MAPDIM
	case y < 0:
		chunkY = y/MAPDIM - 1
		y = MAPDIM + y%MAPDIM
	}
	newX = x
	newY = y
	if chunkX > 1 || chunkX < -1 || chunkY > 1 || chunkY < -1 {
		return 0, 0, 0, 0, errors.New("normalizeCoords: moving away of 3x3 chunk sector")
	}
	return
}

func MoveObject(o Object, ch Chunk, dx, dy int) {
	px, py := o.Coord()

	newX, newY, chX, chY, err := NormalizeCoords(px+dx, py+dy)
	if err != nil {
		log.Print("moveObject: ", err.Error())
		return
	}
	// move inside chunk
	if chX == 0 && chY == 0 {
		o.Move(newX, newY)
		ch.Move(o)
		return
	}
	// move between chunks
	newChunk := ch.Neighbour(chX, chY)
	o.Move(newX, newY)
	o.OnChangeChunk(ch, newChunk)
}
