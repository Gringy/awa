// Package binary provides way to read binary data.
package binary

import (
	"errors"
	"fmt"
)

type Reader struct {
	data []byte
	ptr  int
}

// Creates new binary reader.
func NewReader(data []byte) Reader {
	reader := Reader{
		data: data,
		ptr:  0,
	}
	return reader
}

// Detects if buffer ends.
func (rdr *Reader) Ends() bool {
	return rdr.ptr >= len(rdr.data)
}

func (rdr *Reader) Debug() {
	fmt.Printf("Ptr: %d, ends: %t,  after: %d\n", rdr.ptr, rdr.Ends(), rdr.Count())
}

// Returns count of unreaded bytes
func (rdr *Reader) Count() int {
	return len(rdr.data) - rdr.ptr
}

// Reads signed byte.
func (rdr *Reader) ReadI1() (int8, error) {
	if rdr.Count() >= 1 {
		data := rdr.data[rdr.ptr]
		rdr.ptr++
		return int8(data), nil
	}
	rdr.ptr++
	return 0, errors.New("ReadI1: data slice ends")
}

// Reads unsigned byte.
func (rdr *Reader) ReadByte() (uint8, error) {
	if rdr.Count() >= 1 {
		data := rdr.data[rdr.ptr]
		rdr.ptr++
		return data, nil
	}
	rdr.ptr++
	return 0, errors.New("ReadU1: data slice ends")
}

// Reads signed short.
func (rdr *Reader) ReadI2() (int16, error) {
	if rdr.Count() >= 2 {
		data := int16(rdr.data[rdr.ptr])<<8 + int16(rdr.data[rdr.ptr+1])
		rdr.ptr += 2
		return data, nil
	}
	rdr.ptr += 2
	return 0, errors.New("ReadI2: data slice ends")
}

// Reads unsigned short.
func (rdr *Reader) ReadShort() (uint16, error) {
	if rdr.Count() >= 2 {
		data := uint16(rdr.data[rdr.ptr])<<8 + uint16(rdr.data[rdr.ptr+1])
		rdr.ptr += 2
		return data, nil
	}
	rdr.ptr += 2
	return 0, errors.New("ReadU2: data slice ends")
}

// Read signed 32 bit integer.
func (rdr *Reader) ReadI4() (int32, error) {
	if rdr.Count() >= 4 {
		data := int32(rdr.data[rdr.ptr])<<24 + int32(rdr.data[rdr.ptr+1])<<16 + int32(rdr.data[rdr.ptr+2])<<8 + int32(rdr.data[rdr.ptr+3])
		rdr.ptr += 4
		return data, nil
	}
	rdr.ptr += 4
	return 0, errors.New("ReadI4: data slice ends")
}

// Reads unsigned 32 bit integer.
func (rdr *Reader) ReadInt() (uint32, error) {
	if rdr.Count() >= 4 {
		data := uint32(rdr.data[rdr.ptr])<<24 + uint32(rdr.data[rdr.ptr+1])<<16 + uint32(rdr.data[rdr.ptr+2])<<8 + uint32(rdr.data[rdr.ptr+3])
		rdr.ptr += 4
		return data, nil
	}
	rdr.ptr += 4
	return 0, errors.New("ReadU4: data slice ends")
}

// Reads signed 64 bit integer.
func (rdr *Reader) ReadI8() (int64, error) {
	if rdr.Count() >= 8 {
		data := int64(rdr.data[rdr.ptr])<<56 + int64(rdr.data[rdr.ptr+1])<<48 + int64(rdr.data[rdr.ptr+2])<<40 + int64(rdr.data[rdr.ptr+3])<<32
		data += int64(rdr.data[rdr.ptr+4])<<24 + int64(rdr.data[rdr.ptr+5])<<16 + int64(rdr.data[rdr.ptr+6])<<8 + int64(rdr.data[rdr.ptr+7])
		rdr.ptr += 8
		return data, nil
	}
	rdr.ptr += 8
	return 0, errors.New("ReadI8: data slice ends")
}

// Reads unsigned 64 bit integer.
func (rdr *Reader) ReadLong() (uint64, error) {
	if rdr.Count() >= 8 {
		data := uint64(rdr.data[rdr.ptr])<<56 + uint64(rdr.data[rdr.ptr+1])<<48 + uint64(rdr.data[rdr.ptr+2])<<40 + uint64(rdr.data[rdr.ptr+3])<<32
		data += uint64(rdr.data[rdr.ptr+4])<<24 + uint64(rdr.data[rdr.ptr+5])<<16 + uint64(rdr.data[rdr.ptr+6])<<8 + uint64(rdr.data[rdr.ptr+7])
		rdr.ptr += 8
		return data, nil
	}
	rdr.ptr += 8
	return 0, errors.New("ReadU8: data slice ends")
}

// Reads string from bytes in format:
// 2 bytes - length of string in bytes,
// then - string in utf-8
func (rdr *Reader) ReadString() (string, error) {
	length, err := rdr.ReadShort()
	if err != nil {
		return "", errors.New("ReadString: bad data, can't read length of string")
	}
	if rdr.Count() < int(length) {
		return "", errors.New("ReadString: bad data, too short data, or too big counter")
	}
	str := string(rdr.data[rdr.ptr : rdr.ptr+int(length)])
	rdr.ptr += int(length)
	return str, nil
}

func (rdr *Reader) MustByte() byte {
	ret, err := rdr.ReadByte()
	if err != nil {
		panic(err)
	}
	return ret
}

func (rdr *Reader) MustShort() uint16 {
	ret, err := rdr.ReadShort()
	if err != nil {
		panic(err)
	}
	return ret
}

func (rdr *Reader) MustInt() uint32 {
	ret, err := rdr.ReadInt()
	if err != nil {
		panic(err)
	}
	return ret
}

func (rdr *Reader) MustString() string {
	ret, err := rdr.ReadString()
	if err != nil {
		panic(err)
	}
	return ret
}
