package binary

import (
	"bytes"
)

// Type BinaryWriter
type Writer struct {
	buf bytes.Buffer
}

// NewWriter func returns new binary writer
func NewWriter() Writer {
	return Writer{}
}

func (writer *Writer) WriteByte(b byte) {
	writer.buf.WriteByte(b)
}

func (writer *Writer) WriteBytes(bytes []byte) {
	writer.buf.Write(bytes)
}

func (writer *Writer) WriteShort(s uint16) {
	writer.buf.WriteByte(byte((s & 0xFF00) >> 8))
	writer.buf.WriteByte(byte(s & 0xFF))
}

func (writer *Writer) WriteInt(i uint32) {
	writer.buf.WriteByte(byte((i & 0xFF000000) >> 24))
	writer.buf.WriteByte(byte((i & 0xFF0000) >> 16))
	writer.buf.WriteByte(byte((i & 0xFF00) >> 8))
	writer.buf.WriteByte(byte(i & 0xFF))
}

func (writer *Writer) WriteString(str string) {
	bytes := []byte(str)
	writer.WriteShort(uint16(len(bytes)))
	writer.buf.Write(bytes)
}

// Func GetBytes returns bytes written to slice
func (writer *Writer) GetBytes() []byte {
	return writer.buf.Bytes()
}
