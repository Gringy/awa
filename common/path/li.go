package path

import (
	"bitbucket.org/gringy/awa/common"
)

type Li struct {
	size       int
	cells      []int
	points     []common.Point
	pointsSize int
	start      common.Point
	chunk      common.Chunk
	current    int
}

const (
	UNCHECKED = -1
	WALL      = -2
)

func getWalkable(chunk common.Chunk, x, y int) bool {
	newX, newY, chX, chY, err := common.NormalizeCoords(x, y)
	if err != nil {
		return false
	}
	chunk = chunk.Neighbour(chX, chY)
	if chunk == nil {
		return false
	}
	tp := chunk.Map().GetAt(newX, newY)
	return common.MainHub.World.TypeManager().TypeById(tp.Type).Walkable
}

func (li *Li) SetAt(x, y int, val int) {
	if x < -li.size || x > li.size || y < -li.size || y > li.size {
		return
	}
	idx := (y+li.size)*(li.size*2+1) + x + li.size
	li.cells[idx] = val
}

func (li *Li) GetAt(x, y int) int {
	if x < -li.size || x > li.size || y < -li.size || y > li.size {
		return WALL
	}
	idx := (y+li.size)*(li.size*2+1) + x + li.size
	return li.cells[idx]
}

func (li *Li) Move() {
	for x := -li.size; x <= li.size; x++ {
		for y := -li.size; y <= li.size; y++ {
			tp := li.GetAt(x, y)
			if tp == li.current {
				if li.GetAt(x, y+1) == UNCHECKED {
					li.SetAt(x, y+1, li.current+1)
				}
				if li.GetAt(x, y-1) == UNCHECKED {
					li.SetAt(x, y-1, li.current+1)
				}
				if li.GetAt(x+1, y) == UNCHECKED {
					li.SetAt(x+1, y, li.current+1)
				}
				if li.GetAt(x-1, y) == UNCHECKED {
					li.SetAt(x-1, y, li.current+1)
				}
			}
		}
	}
	li.current++
}

func (li *Li) MoveN(n int) {
	for i := 0; i < n; i++ {
		li.Move()
	}
}

func (li *Li) IsPassedBy(x, y, n int) bool {
	tp := li.GetAt(x, y)
	if tp != WALL && tp != UNCHECKED && tp <= n {
		return true
	}
	return false
}

func (li *Li) MoveObjectTo(o common.Object, ch common.Chunk, to common.Point) bool {
	if tp := li.GetAt(to.X, to.Y); tp == UNCHECKED || tp == WALL {
		return false
	}
	speed := o.Speed()
	tp := li.GetAt(to.X, to.Y)
	for tp > speed {
		tp = li.GetAt(to.X, to.Y)
		switch {
		case li.GetAt(to.X+1, to.Y) == tp-1:
			to = common.Point{to.X + 1, to.Y}
		case li.GetAt(to.X-1, to.Y) == tp-1:
			to = common.Point{to.X - 1, to.Y}
		case li.GetAt(to.X, to.Y+1) == tp-1:
			to = common.Point{to.X, to.Y + 1}
		case li.GetAt(to.X, to.Y-1) == tp-1:
			to = common.Point{to.X, to.Y - 1}
		default:
			return false
		}
	}
	common.MoveObject(o, ch, to.X, to.Y)
	return true
}

func (li *Li) Clear() {
	for x := -li.size; x <= li.size; x++ {
		for y := -li.size; y <= li.size; y++ {
			li.SetAt(x, y, UNCHECKED)
		}
	}
	li.SetAt(0, 0, 0)
	li.current = 0
}

func NewEmptyLi(size int) *Li {
	li := &Li{
		size:  size,
		cells: make([]int, (size*2+1)*(size*2+1)),
	}
	li.Clear()
	return li
}

func NewLi(size int, chunk common.Chunk, start common.Point) *Li {
	li := &Li{
		size:  size,
		cells: make([]int, (size*2+1)*(size*2+1)),
		start: start,
		chunk: chunk,
	}

	if chunk != nil {
		for x := -size; x <= size; x++ {
			for y := -size; y <= size; y++ {
				if getWalkable(chunk, start.X+x, start.Y+y) {
					li.SetAt(x, y, UNCHECKED)
				} else {
					li.SetAt(x, y, WALL)
				}
			}
		}
	} else {
		for x := -size; x <= size; x++ {
			for y := -size; y <= size; y++ {
				li.SetAt(x, y, UNCHECKED)
			}
		}
	}

	li.SetAt(0, 0, 0)

	return li
}
