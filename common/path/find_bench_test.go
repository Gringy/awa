package path

import (
	"math/rand"
	"testing"

	"bitbucket.org/gringy/awa/common"
)

const (
	fsz = 20
)

func BenchmarkAstar(b *testing.B) {
	failed := 0
	for n := 0; n < b.N; n++ {
		a := NewAstar(fsz, nil, common.Point{}, common.Point{fsz, fsz})
		for i := 0; i < fsz*4; i++ {
			x := rand.Intn(2*fsz) - fsz
			y := rand.Intn(2*fsz) - fsz
			if (x != 0 || y != 0) && (x != fsz || y != fsz) {
				p := a.GetPoint(x, y)
				if p != nil {
					p.tp = UNWALKED
				}
			}
		}
		n := 0
		for a.GetTp(fsz, fsz) != CLOSED && n < 32*fsz {
			a.Step()
			n++
		}
		if a.GetTp(fsz, fsz) != CLOSED {
			failed++
		}
	}
	b.ReportAllocs()
	b.Logf("Astar: total=%d, failed=%d", b.N, failed)
}

func BenchmarkLi(b *testing.B) {
	failed := 0
	for n := 0; n < b.N; n++ {
		li := NewLi(fsz, nil, common.Point{})
		for i := 0; i < fsz*4; i++ {
			x := rand.Intn(2*fsz) - fsz
			y := rand.Intn(2*fsz) - fsz
			if (x != 0 || y != 0) && (x != fsz || y != fsz) {
				li.SetAt(x, y, WALL)
			}
		}
		n := 0
		for {
			tp := li.GetAt(fsz, fsz)
			if tp != WALL && tp != UNCHECKED || n >= 8*fsz {
				break
			}
			li.Move()
			n++
		}
		tp := li.GetAt(fsz, fsz)
		if tp == WALL || tp == UNCHECKED {
			failed++
		}
	}
	b.ReportAllocs()
	b.Logf("Li: total=%d, failed=%d", b.N, failed)
}
