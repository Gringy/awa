package path

import (
	"fmt"

	"bitbucket.org/gringy/awa/common"
)

type astarTp int

const (
	NOWHERE astarTp = iota
	CLOSED
	OPENED
	UNWALKED
)

type astarPoint struct {
	x, y   int
	weight int
	steps  int
	tp     astarTp
	prev   *astarPoint
}

func (p *astarPoint) recalc(other *astarPoint, a *Astar) {
	sx := a.end.X
	sy := a.end.Y
	switch p.tp {
	case NOWHERE:
		a.addOpened(p)
		p.steps = other.steps + 1
		p.weight = common.Abs(p.x-sx) + common.Abs(p.y-sy) + p.steps
		p.prev = other
	case OPENED:
		if p.steps < other.steps {
			other.steps = p.steps + 1
			other.weight = common.Abs(other.x-sx) + common.Abs(other.y-sy) + other.steps
			other.prev = p
		}
	}
}

type Astar struct {
	size   int
	field  []astarPoint
	opened []*astarPoint
	closed []*astarPoint
	start  common.Point
	end    common.Point
	chunk  common.Chunk
}

func (a *Astar) Step() {
	min := a.size * 10000
	idx := -1
	for i, p := range a.opened {
		if p.weight <= min {
			idx = i
			min = p.weight
		}
	}
	if idx == -1 {
		return
	}
	//fmt.Println("Checked: ", a.opened[idx])
	p := a.opened[idx]
	a.removeOpened(idx)
	a.addClosed(p)
	if np := a.GetPoint(p.x+1, p.y); np != nil && np.tp == NOWHERE {
		np.recalc(p, a)
	}
	if np := a.GetPoint(p.x-1, p.y); np != nil && np.tp == NOWHERE {
		np.recalc(p, a)
	}
	if np := a.GetPoint(p.x, p.y+1); np != nil && np.tp == NOWHERE {
		np.recalc(p, a)
	}
	if np := a.GetPoint(p.x, p.y-1); np != nil && np.tp == NOWHERE {
		np.recalc(p, a)
	}
}

func (a *Astar) StepN(n int) {
	for i := 0; i < n; i++ {
		a.Step()
	}
}

func (a *Astar) debugPrint() {
	for y := a.size; y >= -a.size; y-- {
		for x := -a.size; x <= a.size; x++ {
			p := a.GetPoint(x, y)
			if p != nil && p.x == a.end.X && p.y == a.end.Y {
				fmt.Print("X")
				continue
			}
			tp := a.GetTp(x, y)

			switch tp {
			case OPENED:
				fmt.Print("0")
			case CLOSED:
				fmt.Print("C")
			case NOWHERE:
				fmt.Print(" ")
			case UNWALKED:
				fmt.Print("#")
			}
		}
		fmt.Println()
	}
}

func (a *Astar) addClosed(point *astarPoint) {
	a.closed = append(a.closed, point)
	point.tp = CLOSED
}

func (a *Astar) addOpened(point *astarPoint) {
	a.opened = append(a.opened, point)
	point.tp = OPENED
}

func (a *Astar) removeOpened(i int) {
	if i >= len(a.opened) {
		panic("i >= len(a.opened)")
	}
	a.opened[i] = a.opened[len(a.opened)-1]
	a.opened = a.opened[:len(a.opened)-1]
}

func (a *Astar) SetWall(x, y int) {
	if x < -a.size || x > a.size || y < -a.size || y > a.size {
		return
	}
	idx := (y+a.size)*(a.size*2+1) + x + a.size
	a.field[idx].tp = UNWALKED
}

func (a *Astar) GetTp(x, y int) astarTp {
	if x < -a.size || x > a.size || y < -a.size || y > a.size {
		return UNWALKED
	}
	idx := (y+a.size)*(a.size*2+1) + x + a.size
	return a.field[idx].tp
}

func (a *Astar) GetPoint(x, y int) *astarPoint {
	if x < -a.size || x > a.size || y < -a.size || y > a.size {
		return nil
	}
	idx := (y+a.size)*(a.size*2+1) + x + a.size
	return &a.field[idx]
}

func (a *Astar) MoveObjectTo(o common.Object, ch common.Chunk, to common.Point) bool {
	// if point is away from field or way not found
	if p := a.GetPoint(to.X, to.Y); p == nil || p.tp != CLOSED {
		return false
	}
	// move from end of way till we can move to this cell
	speed := o.Speed()
	p := a.GetPoint(to.X, to.Y)
	for p.steps > speed {
		if p.prev != nil {
			p = p.prev
		} else {
			return false
		}
	}
	// moving
	common.MoveObject(o, ch, p.x, p.y)
	return true
}

func NewAstar(size int, chunk common.Chunk, start, end common.Point) *Astar {
	astar := &Astar{
		size:   size,
		field:  make([]astarPoint, (size*2+1)*(size*2+1)),
		opened: make([]*astarPoint, 0, size*4),
		closed: make([]*astarPoint, 0, size*size*2),
		start:  start,
		chunk:  chunk,
		end:    end,
	}
	for x := -size; x <= size; x++ {
		for y := -size; y <= size; y++ {
			p := astar.GetPoint(x, y)
			p.x = x
			p.y = y
			p.tp = NOWHERE
		}
	}
	if chunk == nil {
		astar.addOpened(astar.GetPoint(0, 0))
		return astar
	}
	for x := -size; x <= size; x++ {
		for y := -size; y <= size; y++ {
			if !getWalkable(chunk, start.X+x, start.Y+y) {
				astar.addOpened(astar.GetPoint(0, 0))
				astar.SetWall(x, y)
			}
		}
	}
	astar.addOpened(astar.GetPoint(0, 0))
	return astar
}
