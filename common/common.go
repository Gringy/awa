// Package common contains all interfaces and utils used in most of other packages.
package common

// Config struct contains variables loaded from json config file
type Config struct {
	Port       string `json:"port"`
	TurnMillis int    `json:"turn_millis"`
	NXN        int    `json:"nxn"`
	Players    int    `json:"players"`
}

// Net interface
type Net interface {
	Start()
}

// World interface
type World interface {
	Start()
	NextGameID() ID
	Registered(Client)
	Unregistered(Client)
	Command(Command)
	PlayerMovedTo(player PlayerID, to ChunkID)
	TypeManager() TypeManager
	Chunks() []Chunk
	Chunk(ChunkID) (Chunk, error)
}

// Hub struct
type Hub struct {
	World  World
	Net    Net
	Config Config
}

var MainHub *Hub

// IDs
type ID uint32
type ChunkID uint32
type PlayerID uint32

type ObjectType uint16
type BlockType uint16

// Command interface
type Command interface {
	Do(Player, Chunk)
	PlayerID() PlayerID
}

// Dharma interface
type Dharma interface {
	Bytes() []byte
}

// Message struct
type Message struct {
	Bytes []byte
}

// Chunk interface
type Chunk interface {
	Tick()

	ChunkID() ChunkID

	Map() *Map
	Neighbour(dx, dy int) Chunk
	Neighbours() []Chunk
	SetNeighbour(dx, dy int, nei Chunk)

	GetObject(id ID) (Object, error)
	AddObject(Object)
	RemoveObject(obj Object)

	GetPlayer(playerID PlayerID) (Player, error)
	AddPlayer(Player)

	Objects() []Object

	// Dharmas
	Dharmas() []Dharma
	SendDelta()
	ClearDelta()

	Move(obj Object)
	Add(obj Object)
	Del(obj Object)
	Say(obj Object, msg string)
	Replace(x, y int, tp uint16)
	ChangeType(obj Object)
}

type ObjectDrawIndex struct {
	ObjectType ObjectType
	DrawIndex  byte
}

// Object interface
type Object interface {
	Tick(Chunk)

	ID() ID
	Type() ObjectType
	DrawIndexes() []ObjectDrawIndex
	Speed() int

	Coord() (x, y int)
	Move(x, y int)
	Params() Params

	Action(ActionDesc)
	OnChangeChunk(old, new Chunk)
	OnItemsShow(ItemsHolder)
}

// Player interface
type Player interface {
	Object

	SayMsg(ch Chunk, msg string)
	MoveMsg(ch Chunk, dx, dy int)
	ReplaceMsg(ch Chunk, dx, dy int, tp uint16)
	BlockRequestMsg(ch Chunk, ids []uint16)
	ActionRequestMsg(Chunk, Action, ChunkID, ID)
	ItemMoveMsg(ch Chunk, fromID ID, fromPoint Point, toID ID, toPoint Point)
	ItemActionMsg()
	ItemDescRequestMsg(ch Chunk, ids []uint32)

	ExtraDharmas() []Dharma
	ClearExtraDharmas()

	Client() Client
	SetClient(Client)

	PlayerID() PlayerID
}

type ItemsHolder interface {
	Size() Point
	Volume() int
	ObjectID() ID
	GetAt(x, y int) Item
	SetAt(x, y int, item Item)
	AddListener(HolderListener)
	RemoveListener(HolderListener)
}

type HolderListener interface {
	OnHolderChange(Object, ItemsHolder, Point, Item)
}

type HolderObject interface {
	ItemsHolder
	Object
}

// Client interface
type Client interface {
	PlayerID() PlayerID
	Login(*Hub) (id PlayerID, err error)
	Send(Message)
	Error()
}
