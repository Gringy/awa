package common

import (
	"log"

	"bitbucket.org/gringy/awa/common/binary"
)

const (
	// MAPDIM is dimentions of map
	MAPDIM = 32
)

type Block struct {
	Type BlockType
}

func (b Block) IsEmpty() bool {
	return b.Type == 0
}

type Map struct {
	arr [MAPDIM * MAPDIM]Block
}

func (m *Map) GetAt(x, y int) Block {
	if x < 0 || y < 0 || x >= MAPDIM || y >= MAPDIM {
		log.Panicf("Bad coords: (%d, %d)", x, y)
	}
	return m.arr[y*MAPDIM+x]
}

func (m *Map) SetAt(x, y int, t Block) {
	if x < 0 || y < 0 || x >= MAPDIM || y >= MAPDIM {
		log.Panicf("Bad coords: (%d, %d)", x, y)
	}
	m.arr[y*MAPDIM+x] = t
}

func (m *Map) SetTypeAt(x, y int, tp BlockType) {
	if x < 0 || y < 0 || x >= MAPDIM || y >= MAPDIM {
		log.Panicf("Bad coords: (%d, %d)", x, y)
	}
	m.arr[y*MAPDIM+x].Type = tp
}

func (m *Map) GetBytes() []byte {
	writer := binary.NewWriter()
	for _, block := range m.arr {
		writer.WriteShort(uint16(block.Type))
	}
	return writer.GetBytes()
}
