package common

import (
	"errors"
	"log"
	"runtime"
)

func GetRoutines() int {
	return runtime.NumGoroutine()
}

func Normalize(n, brd int) int {
	if n < 0 {
		return n + brd
	}
	if n >= brd {
		return n - brd
	}
	return n
}

func ChangeItems(from ItemsHolder, fromPoint Point, to ItemsHolder, toPoint Point) error {
	fromSize := from.Size()
	toSize := to.Size()
	if fromPoint.X < 0 || fromPoint.Y < 0 || fromPoint.X >= fromSize.X || fromPoint.Y >= fromSize.Y {
		return errors.New("common.ChangeItems: fromPoint is invalid")
	}
	if toPoint.X < 0 || toPoint.Y < 0 || toPoint.X >= toSize.X || toPoint.Y >= toSize.Y {
		return errors.New("common.ChangeItems: toPoint is invalid")
	}
	fromItem := from.GetAt(fromPoint.X, fromPoint.Y)
	toItem := to.GetAt(toPoint.X, toPoint.Y)
	if fromItem.ID == 0 {
		// nothing to move
		return nil
	}
	if toItem.ID == fromItem.ID {
		// try to stack items
		tp := MainHub.World.TypeManager().ItemById(toItem.ID)
		if tp == nil {
			return errors.New("common.ChangeItems: unknown id when stacking items")
		}
		if toItem.Count >= tp.Stack {
			// dest item is full
			log.Printf("dest item is full: toItem.Count:%d tp.Stack:%d", toItem.Count, tp.Stack)
			return nil
		}
		var toCount, fromCount, toQuality uint8
		toCount = fromItem.Count + toItem.Count
		if toCount > tp.Stack {
			fromCount = toCount - tp.Stack
			toCount = tp.Stack
		}
		if toItem.Quality < fromItem.Quality {
			toQuality = toItem.Quality
		} else {
			toQuality = fromItem.Quality
		}
		fromItem.Count = fromCount
		// if fromCount == 0 {
		// 	fromItem = Item{}
		// } else {
		//
		// }
		toItem.Count = toCount
		toItem.Quality = toQuality
		if fromCount != 0 {
			from.SetAt(fromPoint.X, fromPoint.Y, fromItem)
		} else {
			from.SetAt(fromPoint.X, fromPoint.Y, Item{})
		}
		to.SetAt(toPoint.X, toPoint.Y, toItem)
		return nil
	}
	// swap items
	from.SetAt(fromPoint.X, fromPoint.Y, toItem)
	to.SetAt(toPoint.X, toPoint.Y, fromItem)
	return nil
}
