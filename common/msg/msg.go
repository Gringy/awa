// Package msg
package msg

import (
	"log"

	"bitbucket.org/gringy/awa/common"
	"bitbucket.org/gringy/awa/common/binary"
)

func InitMessage(player common.Player, chunk common.Chunk) []byte {
	writer := binary.NewWriter()
	// Message type
	writer.WriteByte(0x00)
	// Player's game ID
	writer.WriteInt(uint32(player.ID()))
	// Tick Time
	writer.WriteShort(uint16(common.MainHub.Config.TurnMillis))
	// Params
	writeParams(&writer, player.Params())
	// Chunks
	for y := -1; y <= 1; y++ {
		for x := -1; x <= 1; x++ {
			currentChunk := chunk.Neighbour(x, y)
			if currentChunk != nil {
				writeChunk(&writer, currentChunk)
			} else {
				// if it is no chunk here
				// then ID of chunk is -1
				writer.WriteInt(0xFFFFFFFF)
				log.Print("InitMessage: nil chunk!!!")
			}
		}
	}
	return writer.GetBytes()
}

func writeParams(writer *binary.Writer, params common.Params) {
	writer.WriteShort(params.HP)
	writer.WriteShort(params.EP)
	writer.WriteShort(params.STR)
	writer.WriteShort(params.DEX)
	writer.WriteShort(params.WIS)
}

func writeChunk(writer *binary.Writer, chunk common.Chunk) {
	// ID of chunk
	writer.WriteInt(uint32(chunk.ChunkID()))
	// Map of chunk
	writer.WriteBytes(chunk.Map().GetBytes())
	chunkObjects := chunk.Objects()
	// Count of objects of chunk
	writer.WriteShort(uint16(len(chunkObjects)))
	for _, obj := range chunkObjects {
		writeObject(writer, obj)
	}
}

func writeObject(writer *binary.Writer, object common.Object) {
	writer.WriteInt(uint32(object.ID()))
	// Object type
	writer.WriteShort(uint16(object.Type()))
	// Draw Indexes
	indexes := object.DrawIndexes()
	writer.WriteShort(uint16(len(indexes)))
	for _, index := range indexes {
		writer.WriteShort(uint16(index.ObjectType))
		writer.WriteByte(index.DrawIndex)
	}
	// Coords
	x, y := object.Coord()
	// Coordinates of object
	writer.WriteByte(byte(x))
	writer.WriteByte(byte(y))
}

func Byte2signed(b byte) int {
	return int(int8(b))
}

func DeltaMessage(dharmas []common.Dharma) []byte {
	writer := binary.NewWriter()
	// Write type
	writer.WriteByte(0x01)
	// Write count of dharmas
	writer.WriteShort(uint16(len(dharmas)))
	// Write dharmas
	for i := range dharmas {
		writer.WriteBytes(dharmas[i].Bytes())
	}
	return writer.GetBytes()
}

func DeltaMessageFromTwo(dharmas1, dharmas2 []common.Dharma) []byte {
	writer := binary.NewWriter()
	// Write type
	writer.WriteByte(0x01)
	// Write count of dharmas
	writer.WriteShort(uint16(len(dharmas1) + len(dharmas2)))
	// Write dharmas
	for i := range dharmas1 {
		writer.WriteBytes(dharmas1[i].Bytes())
	}
	for i := range dharmas2 {
		writer.WriteBytes(dharmas2[i].Bytes())
	}
	return writer.GetBytes()
}

func ChangeMessage(old, new common.Chunk) []byte {
	writer := binary.NewWriter()
	// WriteType
	writer.WriteByte(0x02)
	// get chunks which client has
	oldChunks := old.Neighbours()
	// get chunks which client want to have
	newChunks := new.Neighbours()
	// write IDs of new chunks
	for _, chunk := range newChunks {
		if chunk != nil {
			writer.WriteInt(uint32(chunk.ChunkID()))
		} else {
			writer.WriteInt(0xFFFFFFFF)
		}
	}
	// write missing chunks
	for _, newChunk := range newChunks {
		found := false
		for _, oldChunk := range oldChunks {
			if oldChunk.ChunkID() == newChunk.ChunkID() {
				found = true
				break
			}
		}
		if !found {
			writeChunk(&writer, newChunk)
		}
	}
	return writer.GetBytes()
}
