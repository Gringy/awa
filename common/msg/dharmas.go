package msg

import (
	"bitbucket.org/gringy/awa/common"
	"bitbucket.org/gringy/awa/common/binary"
)

type MoveDharma struct {
	GameID common.ID
	X, Y   int
}

func (md *MoveDharma) Bytes() []byte {
	writer := binary.NewWriter()
	// type of dharma
	writer.WriteByte(0x00)
	// id of subject
	writer.WriteInt(uint32(md.GameID))
	// x
	writer.WriteByte(uint8(md.X))
	// y
	writer.WriteByte(uint8(md.Y))
	return writer.GetBytes()
}

type SayDharma struct {
	GameID common.ID
	Msg    string
}

func (dd *SayDharma) Bytes() []byte {
	writer := binary.NewWriter()
	writer.WriteByte(0x01)
	writer.WriteInt(uint32(dd.GameID))
	writer.WriteString(dd.Msg)
	return writer.GetBytes()
}

type AddDharma struct {
	ChunkID common.ChunkID
	Obj     common.Object
}

func (ad *AddDharma) Bytes() []byte {
	writer := binary.NewWriter()
	// type of dharma
	writer.WriteByte(0x02)
	// id of chunk
	writer.WriteInt(uint32(ad.ChunkID))
	writeObject(&writer, ad.Obj)
	return writer.GetBytes()
}

type DelDharma struct {
	ChunkID common.ChunkID
	GameID  common.ID
}

func (dd *DelDharma) Bytes() []byte {
	writer := binary.NewWriter()
	writer.WriteByte(0x03)
	writer.WriteInt(uint32(dd.ChunkID))
	writer.WriteInt(uint32(dd.GameID))
	return writer.GetBytes()
}

type ReplaceDharma struct {
	ChunkID common.ChunkID
	X, Y    byte
	Tp      uint16
}

func (rd *ReplaceDharma) Bytes() []byte {
	writer := binary.NewWriter()
	writer.WriteByte(0x04)
	writer.WriteInt(uint32(rd.ChunkID))
	writer.WriteByte(rd.X)
	writer.WriteByte(rd.Y)
	writer.WriteShort(rd.Tp)
	return writer.GetBytes()
}

type BlockDescriptionDharma struct {
	Tp        common.BlockType
	DrawIndex byte
	Passable  bool
	Name      string
}

func (bdd *BlockDescriptionDharma) Bytes() []byte {
	writer := binary.NewWriter()
	writer.WriteByte(0x05)
	writer.WriteShort(uint16(bdd.Tp))
	writer.WriteByte(bdd.DrawIndex)
	if bdd.Passable {
		writer.WriteByte(0x01)
	} else {
		writer.WriteByte(0x00)
	}
	writer.WriteString(bdd.Name)
	return writer.GetBytes()
}

type HolderDharma struct {
	Holder common.ItemsHolder
	GameID common.ID
}

func (hd *HolderDharma) Bytes() []byte {
	writer := binary.NewWriter()
	writer.WriteByte(0x06)
	writer.WriteInt(uint32(hd.GameID))
	size := hd.Holder.Size()
	writer.WriteByte(byte(size.X))
	writer.WriteByte(byte(size.Y))
	for y := 0; y < size.Y; y++ {
		for x := 0; x < size.X; x++ {
			item := hd.Holder.GetAt(x, y)
			writer.WriteInt(uint32(item.ID))
			writer.WriteByte(item.Count)
			writer.WriteByte(item.Quality)
		}
	}
	return writer.GetBytes()
}

type HolderChangeDharma struct {
	GameID common.ID
	Point  common.Point
	Item   common.Item
}

func (hcd *HolderChangeDharma) Bytes() []byte {
	writer := binary.NewWriter()
	writer.WriteByte(0x07)
	writer.WriteInt(uint32(hcd.GameID))
	writer.WriteByte(byte(hcd.Point.X))
	writer.WriteByte(byte(hcd.Point.Y))
	item := &hcd.Item
	writer.WriteInt(uint32(item.ID))
	writer.WriteByte(item.Count)
	writer.WriteByte(item.Quality)
	return writer.GetBytes()
}

type HolderCloseDharma struct {
	GameID common.ID
}

func (hcd *HolderCloseDharma) Bytes() []byte {
	writer := binary.NewWriter()
	writer.WriteByte(0x08)
	writer.WriteInt(uint32(hcd.GameID))
	return writer.GetBytes()
}

type ItemDescDharma struct {
	Type      common.ItemType
	Grp       common.ItemGroup
	Name      string
	Stack     uint8
	DrawIndex uint16
}

func (idd *ItemDescDharma) Bytes() []byte {
	writer := binary.NewWriter()
	writer.WriteByte(0x09)
	writer.WriteInt(uint32(idd.Type))
	writer.WriteInt(uint32(idd.Grp))
	writer.WriteString(idd.Name)
	writer.WriteByte(idd.Stack)
	writer.WriteShort(idd.DrawIndex)
	return writer.GetBytes()
}

type ParamsChangedDharma struct {
	Params common.Params
}

func (pcd *ParamsChangedDharma) Bytes() []byte {
	writer := binary.NewWriter()
	writer.WriteByte(0x0A)
	writer.WriteShort(pcd.Params.HP)
	writer.WriteShort(pcd.Params.EP)
	writer.WriteShort(pcd.Params.STR)
	writer.WriteShort(pcd.Params.DEX)
	writer.WriteShort(pcd.Params.WIS)
	return writer.GetBytes()
}

type ObjectTypeChangeDharma struct {
	Id   common.ID
	Type common.ObjectType
}

func (otcd *ObjectTypeChangeDharma) Bytes() []byte {
	writer := binary.NewWriter()
	writer.WriteByte(0x0B)
	writer.WriteInt(uint32(otcd.Id))
	writer.WriteShort(uint16(otcd.Type))
	return writer.GetBytes()
}
