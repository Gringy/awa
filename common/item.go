package common

type ItemType uint32

type ItemGroup uint32

type Item struct {
	ID      ItemType
	Count   uint8
	Quality uint8
}

type ItemDesc struct {
	Name      string
	Stack     uint8
	Type      ItemType
	Group     ItemGroup
	DrawIndex uint16
}

const (
	G_NONE ItemGroup = iota
	G_FOOD
	G_SEED
	G_MATERIAL
	G_WEAR
	G_WEAPON
)

func StringToItemGroup(name string) ItemGroup {
	switch name {
	case "food":
		return G_FOOD
	case "seed":
		return G_SEED
	case "material":
		return G_MATERIAL
	case "wear":
		return G_WEAR
	case "weapon":
		return G_WEAPON
	case "none":
		fallthrough
	default:
		return G_NONE
	}
}
