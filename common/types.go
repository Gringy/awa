package common

type TypeDesc struct {
	Id        BlockType
	Names     []string
	Walkable  bool
	DrawIndex byte
}

type TypeManager interface {
	TypeByName(name string) *TypeDesc
	TypeById(id BlockType) *TypeDesc
	TypesMap() map[BlockType]*TypeDesc

	ItemByName(name string) *ItemDesc
	ItemById(id ItemType) *ItemDesc
	ItemsMap() map[ItemType]*ItemDesc
}

const (
	OT_SELF ObjectType = iota
	OT_MOB
	OT_MOB_DEAD
	OT_PLAYER
	OT_CHEST
	OT_TREE
	OT_DOOR_OPENED
	OT_DOOR_CLOSED
	OT_BUILDING
)
