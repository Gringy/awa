package common

type Action uint8

const (
	A_NONE Action = iota
	A_OPEN
	A_CLOSE
	A_WATCH
	A_DROP
	A_KICK
	A_DESTROY
	A_PICK
)

type ActionDesc struct {
	Action   Action
	WhereYou Chunk
	From     Object
}

var ActionsForType = map[ObjectType][]Action{
	OT_MOB: []Action{
		A_KICK,
	},
	OT_MOB_DEAD: []Action{
		A_DESTROY,
	},
	OT_DOOR_OPENED: []Action{
		A_CLOSE,
		A_DESTROY,
	},
	OT_DOOR_CLOSED: []Action{
		A_OPEN,
		A_DESTROY,
	},
	OT_CHEST: []Action{
		A_OPEN,
		A_DESTROY,
	},
	OT_PLAYER: []Action{
		A_KICK,
	},
	OT_SELF: []Action{
		A_OPEN,
		A_WATCH,
	},
	OT_TREE: []Action{
		A_PICK,
		A_DESTROY,
	},
	OT_BUILDING: []Action{
		A_DESTROY,
	},
}
