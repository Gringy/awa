package main

import (
	"github.com/veandco/go-sdl2/sdl"

	"bitbucket.org/gringy/awa/cmd/awa_sdl/gui"
	"bitbucket.org/gringy/awa/common"
)

func (page *GamePage) OnHolderAdded(holder *Holder) {
	vBox := gui.NewVBox(nil, 0xFF222222)
	var window *gui.Window
	window = gui.NewWindow(page.gui, gui.NewButton(page.game.font, "X", func() {
		page.gui.RemoveWidget(window)
		window.Free()
	}), vBox)
	holderWindow := &HolderWindow{
		window: window,
		holder: holder,
		items:  make([]*gui.ItemWidget, holder.size.X*holder.size.Y),
	}
	for y := 0; y < holder.size.Y; y++ {
		widgets := make([]gui.Widget, holder.size.X)
		for x := 0; x < holder.size.X; x++ {
			px := x
			py := y
			img := gui.NewItemWidget(page.game.font, sdl.Color{0xFF, 0xFF, 0xFF, 0xFF}, nil)
			img.SetClickedFunc(func() {
				if !page.state.HasPickedItem() {
					if holder.items[py*holder.size.X+px].tp != 0 {
						holder.selected = &common.Point{px, py}
						page.draggedItem = img.ImageRect()
					}
				} else {
					page.state.SendItemMove(holder, common.Point{px, py})
					page.state.DropPickedItem()
					page.draggedItem = nil
				}
			})
			widgets[x] = img
			//holderWindow.items = append(holderWindow.items, img)
			holderWindow.items[y*holder.size.X+x] = img
		}
		hBox := gui.NewFilledHBox(nil, widgets)
		vBox.AddWidget(hBox)
	}
	holderWindow.Update(page)
	page.AddHolderWindow(holderWindow)
	page.gui.AddWidget(window)

	windowSize := window.Size()
	window.SetPosition(sdl.Point{page.game.surface.W/2 - windowSize.X/2, page.game.surface.H/2 - windowSize.Y/2})
}

type ActionDesc struct {
	act  common.Action
	desc string
}

var actionDescs = []ActionDesc{
	ActionDesc{act: common.A_KICK, desc: "Kick"},
	ActionDesc{act: common.A_OPEN, desc: "Open"},
	ActionDesc{act: common.A_CLOSE, desc: "Close"},
	ActionDesc{act: common.A_DROP, desc: "Drop"},
	ActionDesc{act: common.A_WATCH, desc: "Watch"},
	ActionDesc{act: common.A_DESTROY, desc: "Destroy"},
	ActionDesc{act: common.A_PICK, desc: "Pick"},
}

func (page *GamePage) ShowActionList(x, y int32, tp uint16) {
	itemActions, ok := common.ActionsForType[common.ObjectType(tp)]
	if !ok {
		return
	}
	buttons := make([]gui.Widget, len(itemActions))
	vbox := gui.NewFilledVBox(func(sdl.Point) sdl.Point {
		return sdl.Point{x, y}
	}, 0, buttons)
	for i, act := range itemActions {
		//act := desc.act
		var desc *ActionDesc
		for _, ad := range actionDescs {
			if ad.act == act {
				desc = &ad
				break
			}
		}
		if desc != nil {
			buttons[i] = gui.NewButton(page.game.font, desc.desc, func() {
				page.state.SendAction(page.idForAction, byte(desc.act))
				page.gui.RemoveWidget(vbox)
				vbox.Free()
				page.actionList = nil
			})
		}
	}
	page.gui.AddWidget(vbox)
	page.actionList = vbox
}

func (page *GamePage) ShowReplaceList(x, y int32) {
	page.pointForAction.X = int32(page.mouse.X)
	page.pointForAction.Y = int32(page.mouse.Y)
	buttons := make([]gui.Widget, len(page.state.blocks))
	vbox := gui.NewFilledVBox(func(arg1 sdl.Point) sdl.Point {
		return sdl.Point{x, y}
	}, 0, buttons)
	i := 0
	for _, desc := range page.state.blocks {
		id := desc.Id
		name := desc.Name
		buttons[i] = gui.NewButton(page.game.font, name, func() {
			page.state.SendReplace(byte(page.pointForAction.X), byte(page.pointForAction.Y), id)
			page.gui.RemoveWidget(vbox)
			vbox.Free()
			page.actionList = nil
		})
		i++
	}
	page.gui.AddWidget(vbox)
	page.actionList = vbox
}

func (page *GamePage) ShowBlockList(x, y int32) {
	buttons := make([]gui.Widget, 1)
	vbox := gui.NewFilledVBox(func(sdl.Point) sdl.Point {
		return sdl.Point{x, y}
	}, 0, buttons)
	buttons[0] = gui.NewButton(page.game.font, "Replace with", func() {
		page.gui.RemoveWidget(vbox)
		vbox.Free()
		page.actionList = nil
		page.ShowReplaceList(x, y)
	})
	page.gui.AddWidget(vbox)
	page.actionList = vbox
}
