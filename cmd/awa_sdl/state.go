package main

import (
	"fmt"

	"bitbucket.org/gringy/awa/common"
	"bitbucket.org/gringy/awa/common/binary"
	"bitbucket.org/gringy/awa/common/path"
)

type State struct {
	game         *GamePage
	chunks       [9]*Chunk
	holders      []*Holder
	playerGameID uint32
	blocks       map[uint16]*BlockType
	items        map[uint32]*ItemType
	area         *path.Li
	tickTime     uint16

	playerParams common.Params

	blockToRequest     []uint16
	itemsToRequest     []uint32
	requestSendBlocker bool

	initialized bool
}

type BlockType struct {
	Id        uint16
	Name      string
	DrawIndex byte
	Passable  bool
}

type ItemType struct {
	Type      uint32
	Group     uint32
	Name      string
	Stack     uint8
	DrawIndex uint16
}

type Chunk struct {
	id      uint32
	cells   *common.Map
	objects []*Object
}

type Object struct {
	id   uint32
	x, y byte
	tp   uint16
	//drawIndex byte
	indexes []ObjectDI
}

type ObjectDI struct {
	tp uint16
	di uint8
}

type Item struct {
	tp      uint32
	count   byte
	quality byte
}

type Holder struct {
	gameID   uint32
	size     common.Point
	items    []Item
	selected *common.Point
}

func NewState(game *GamePage) *State {
	state := new(State)
	state.game = game
	state.blocks = make(map[uint16]*BlockType)
	state.items = make(map[uint32]*ItemType)

	return state
}

func (state *State) TryToReceive() {
	for len(state.game.input) != 0 {
		message := <-state.game.input
		state.Message(message)
	}
}

func (state *State) AddHolder(holder *Holder) {
	state.holders = append(state.holders, holder)
	state.game.OnHolderAdded(holder)
}

func (state *State) SendItemMove(dstHolder *Holder, dstPoint common.Point) {
	if state.initialized {
		writer := binary.NewWriter()
		writer.WriteByte(0x05)
		found := false
		for _, holder := range state.holders {
			if holder.selected != nil {
				found = true
				writer.WriteInt(holder.gameID)
				writer.WriteByte(byte(holder.selected.X))
				writer.WriteByte(byte(holder.selected.Y))
				break
			}
		}
		if !found {
			return
		}
		writer.WriteInt(dstHolder.gameID)
		writer.WriteByte(byte(dstPoint.X))
		writer.WriteByte(byte(dstPoint.Y))
		state.game.output <- writer.GetBytes()
	}
}

func (state *State) SendBlockRequest() {
	if len(state.blockToRequest) == 0 {
		return
	}
	if state.initialized {
		writer := binary.NewWriter()
		writer.WriteByte(0x03)
		writer.WriteShort(uint16(len(state.blockToRequest)))
		for _, tp := range state.blockToRequest {
			writer.WriteShort(tp)
		}
		state.game.output <- writer.GetBytes()
		state.blockToRequest = nil
		state.requestSendBlocker = true
	}
}

func (state *State) AddItemRequest(id uint32) {
	for _, reqID := range state.itemsToRequest {
		if reqID == id {
			return
		}
	}
	state.itemsToRequest = append(state.itemsToRequest, id)
}

func (state *State) SendItemRequest() {
	if len(state.itemsToRequest) == 0 {
		return
	}
	if state.initialized {
		writer := binary.NewWriter()
		writer.WriteByte(0x07)
		writer.WriteShort(uint16(len(state.itemsToRequest)))
		for _, tp := range state.itemsToRequest {
			writer.WriteInt(tp)
		}
		state.game.output <- writer.GetBytes()
		state.itemsToRequest = nil
	}
}

func (state *State) HasPickedItem() bool {
	for _, holder := range state.holders {
		if holder.selected != nil {
			return true
		}
	}
	return false
}

func (state *State) DropPickedItem() {
	for _, holder := range state.holders {
		if holder.selected != nil {
			holder.selected = nil
		}
	}
}

func (state *State) GetBlockDesc(blockType uint16) *BlockType {
	blockDesc, ok := state.blocks[blockType]
	if ok {
		return blockDesc
	}
	found := false
	for _, tp := range state.blockToRequest {
		if tp == blockType {
			found = true
			break
		}
	}
	if !found && !state.requestSendBlocker {
		state.blockToRequest = append(state.blockToRequest, blockType)
	}
	return nil
}

func (state *State) UpdateArea() {
	if state.area == nil {
		state.area = path.NewEmptyLi(5)
	} else {
		state.area.Clear()
	}
	player := state.GetObjectById(state.playerGameID)
	if player == nil {
		return
	}
	playerChunkX, playerChunkY := state.GetPlayerChunkCoords()
	playerX := playerChunkX*32 + int(player.x)
	playerY := playerChunkY*32 + int(player.y)

	for x := -5; x <= 5; x++ {
		for y := -5; y <= 5; y++ {
			desc := state.GetBlockDesc(state.GetTypeAt(playerX+x, playerY+y))
			if desc != nil && !desc.Passable {
				state.area.SetAt(x, y, path.WALL)
			}
		}
	}
	state.area.MoveN(5)
}

func (state *State) SendMove(dx, dy int) {
	if state.initialized {
		writer := binary.NewWriter()
		writer.WriteByte(0x01)
		writer.WriteByte(byte(dx))
		writer.WriteByte(byte(dy))
		state.game.output <- writer.GetBytes()
	}
}

func (state *State) SendSay(message string) {
	if state.initialized {
		writer := binary.NewWriter()
		writer.WriteByte(0x00)
		writer.WriteString(message)
		state.game.output <- writer.GetBytes()
	}
}

func (state *State) SendAction(id uint32, act byte) {
	for _, chunk := range state.chunks {
		if chunk != nil {
			for _, obj := range chunk.objects {
				if obj.id == id {
					writer := binary.NewWriter()
					writer.WriteByte(0x04)
					writer.WriteByte(act)
					writer.WriteInt(chunk.id)
					writer.WriteInt(obj.id)
					state.game.output <- writer.GetBytes()
					return
				}
			}
		}
	}
}

func (state *State) SendReplace(dx, dy byte, tp uint16) {
	if state.initialized {
		writer := binary.NewWriter()
		writer.WriteByte(0x02)
		writer.WriteByte(dx)
		writer.WriteByte(dy)
		writer.WriteShort(tp)
		state.game.output <- writer.GetBytes()
	}
}

func (state *State) FindObjIn(dx, dy int) *Object {
	x, y := state.RelativeToAbsolute(dx, dy)
	for i, chunk := range state.chunks {
		if chunk != nil {
			chx := i % 3
			chy := i / 3
			for _, obj := range chunk.objects {
				if int(obj.x)+chx*32 == x && int(obj.y)+chy*32 == y {
					return obj
				}
			}
		}
	}
	return nil
}

func (state *State) Validate() {
	totalObjects := 0
	playerFound := false
	for _, chunk := range state.chunks {
		objectsInChunk := 0
		for _, object := range chunk.objects {
			if object != nil {
				totalObjects++
				objectsInChunk++
				if object.id == state.playerGameID {
					playerFound = true
				}
			}
		}
		fmt.Printf("Chunk #%d: total objects: %d\n", chunk.id, objectsInChunk)
	}
	if playerFound {
		fmt.Println("Player found.")
	} else {
		fmt.Println("Player not found.")
	}
}

func (state *State) ReadInit(reader *binary.Reader) {
	state.initialized = true
	playerGameID := reader.MustInt()

	state.tickTime = reader.MustShort()
	state.playerGameID = playerGameID

	p := ReadParams(reader)
	state.playerParams = p

	for i := 0; i < 9; i++ {
		state.chunks[i] = ReadChunk(reader)
	}
	//state.Validate()
}

func ReadParams(reader *binary.Reader) common.Params {
	HP := reader.MustShort()
	EP := reader.MustShort()
	STR := reader.MustShort()
	DEX := reader.MustShort()
	WIS := reader.MustShort()
	return common.Params{
		HP:  HP,
		EP:  EP,
		STR: STR,
		DEX: DEX,
		WIS: WIS,
	}
}

func (state *State) IsInit() bool {
	return state.initialized
}

func (state *State) RelativeToAbsolute(x, y int) (int, int) {
	player := state.GetObjectById(state.playerGameID)
	if player == nil {
		return 0, 0
	}
	playerChunkX, playerChunkY := state.GetPlayerChunkCoords()
	playerX := playerChunkX*32 + int(player.x) + x
	playerY := playerChunkY*32 + int(player.y) + y
	return playerX, playerY
}

func (state *State) GetTypeAt(x, y int) uint16 {
	if x >= 96 || y >= 96 || x < 0 || y < 0 {
		return 0
	}
	chunkX := x / 32
	chunkY := y / 32
	chunk := state.chunks[chunkY*3+chunkX]
	if chunk == nil {
		return 0
	}
	blockX := x % 32
	blockY := y % 32
	return uint16(chunk.cells.GetAt(blockX, blockY).Type)
}

func (state *State) GetObjectById(id uint32) *Object {
	for _, chunk := range state.chunks {
		if chunk != nil {
			for _, object := range chunk.objects {
				if object != nil && object.id == id {
					return object
				}
			}
		}
	}
	return nil
}

func ReadChunk(reader *binary.Reader) *Chunk {
	id, err := reader.ReadInt()
	if err != nil || id == 0xFFFFFFFF {
		fmt.Println(err)
		return nil
	}
	chunk := new(Chunk)
	chunk.id = id

	chunk.cells = ReadMap(reader)
	chunk.objects = ReadObjects(reader)

	return chunk
}

func ReadMap(reader *binary.Reader) *common.Map {
	cells := new(common.Map)
	for y := 0; y < 32; y++ {
		for x := 0; x < 32; x++ {
			tp, err := reader.ReadShort()
			if err != nil {
				fmt.Println(err)
				return nil
			}
			cells.SetTypeAt(x, y, common.BlockType(tp))
		}
	}
	return cells
}

func (state *State) GetPlayerChunkCoords() (x, y int) {
	for i, chunk := range state.chunks {
		if chunk != nil {
			for _, object := range chunk.objects {
				if object != nil && object.id == state.playerGameID {
					return i % 3, i / 3
				}
			}
		}
	}
	return
}

func ReadObject(reader *binary.Reader) *Object {
	id, err := reader.ReadInt()
	if err != nil {
		fmt.Println(err)
		return nil
	}
	objType, err := reader.ReadShort()
	if err != nil {
		fmt.Println(err)
		return nil
	}
	count := reader.MustShort()
	dis := make([]ObjectDI, count)
	for i := 0; i < int(count); i++ {
		tp := reader.MustShort()
		di := reader.MustByte()
		dis[i] = ObjectDI{tp, di}
	}
	// di, err := reader.ReadByte()
	// if err != nil {
	// 	fmt.Println(err)
	// 	return nil
	// }
	x, err := reader.ReadByte()
	if err != nil {
		fmt.Println(err)
		return nil
	}
	y, err := reader.ReadByte()
	if err != nil {
		fmt.Println(err)
		return nil
	}
	object := &Object{
		id:      id,
		x:       x,
		y:       y,
		tp:      objType,
		indexes: dis,
	}
	return object
}

func ReadObjects(reader *binary.Reader) []*Object {
	count, err := reader.ReadShort()
	if err != nil {
		fmt.Println(err)
		return nil
	}
	objects := make([]*Object, count)
	for i := 0; i < int(count); i++ {
		objects[i] = ReadObject(reader)
	}
	return objects
}

func (state *State) MoveObject(id uint32, x, y byte) {
	found := false
	for _, chunk := range state.chunks {
		if chunk != nil {
			for _, object := range chunk.objects {
				if object.id == id {
					object.x = x
					object.y = y
					found = true
				}
			}
		}
	}
	if !found {
		fmt.Printf("moveObject: object not found: #%d\n", id)
	}
}

func (state *State) ReadChange(reader *binary.Reader) {
	var newIDs [9]uint32
	for i := 0; i < 9; i++ {
		chunkID, err := reader.ReadInt()
		if err != nil {
			fmt.Println(err)
			return
		}
		newIDs[i] = chunkID
	}
	oldChunks := state.chunks
	unknown := 0
	for i := 0; i < 9; i++ {
		if newIDs[i] == 0xFFFFFFFF {
			state.chunks[i] = nil
		} else {
			found := false
			for j := 0; j < 9; j++ {
				if newIDs[i] == oldChunks[j].id {
					state.chunks[i] = oldChunks[j]
					found = true
				}
			}
			if !found {
				unknown++
			}
		}
	}
	for i := 0; i < unknown; i++ {
		chunk := ReadChunk(reader)
		for j := 0; j < 9; j++ {
			if newIDs[j] == chunk.id {
				state.chunks[j] = chunk
			}
		}
	}
}

func ReadItem(reader *binary.Reader) (Item, error) {
	tp, err := reader.ReadInt()
	if err != nil {
		return Item{}, err
	}
	count, err := reader.ReadByte()
	if err != nil {
		return Item{}, err
	}
	quality, err := reader.ReadByte()
	if err != nil {
		return Item{}, err
	}
	return Item{
		tp:      tp,
		count:   count,
		quality: quality,
	}, nil
}

func (state *State) DebugMessage() string {
	ret := ""
	if state.chunks[4] != nil {
		ret += fmt.Sprintf("cid:%d ", state.chunks[4].id)
	}
	if player := state.GetObjectById(state.playerGameID); player != nil {
		ret += fmt.Sprintf("x:%d y:%d", player.x, player.y)
	}
	return ret
}

func (state *State) Message(message []byte) {
	reader := binary.NewReader(message)
	tp, err := reader.ReadByte()
	if err != nil {
		fmt.Println(err)
		return
	}
	switch tp {
	case 0x00:
		state.ReadInit(&reader)
		state.game.UpdateParams()
	case 0x01:
		state.ReadDharma(&reader)
	case 0x02:
		state.ReadChange(&reader)
	default:
		fmt.Println("Unknown type of message")
	}
	state.UpdateArea()
	//text.ClearLine(0)
	//text.Print(0, 0, state.DebugMessage(), 0xFF00FF00)
	state.game.debugMsg.SetText(state.DebugMessage())
	state.game.debugMsg.UpdateText()
}
