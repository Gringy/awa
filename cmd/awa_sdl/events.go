package main

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
)

func (page *GamePage) Events() {
	game := page.game
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			page.running = false
		case *sdl.MouseMotionEvent:
			mx := t.X - game.surface.W/2
			if mx < 0 {
				mx -= 32
			}
			mx /= 32
			my := -t.Y + game.surface.H/2
			if my > 0 {
				my += 32
			}
			my /= 32
			page.mouse.X = int(mx)
			page.mouse.Y = int(my)
			page.realMouse.X = int(t.X)
			page.realMouse.Y = int(t.Y)
			page.debugMsg2.SetText(page.GetDebugMessage())
			page.debugMsg2.UpdateText()
			if page.gui.MouseMove(t) {
				continue
			}
		case *sdl.MouseButtonEvent:
			if page.gui.MouseEvent(t) {
				break
			}
			if page.state.HasPickedItem() && t.Type == sdl.MOUSEBUTTONUP {
				page.state.DropPickedItem()
				page.draggedItem = nil
				break
			}
			if t.Button == sdl.BUTTON_LEFT && t.Type == sdl.MOUSEBUTTONUP {
				page.state.SendMove(page.mouse.X, page.mouse.Y)
			}
			if t.Button == sdl.BUTTON_RIGHT && t.Type == sdl.MOUSEBUTTONUP {
				if page.actionList == nil {
					if obj := page.state.FindObjIn(page.mouse.X, page.mouse.Y); obj != nil {
						page.idForAction = obj.id
						page.ShowActionList(t.X, t.Y, obj.tp)
					} else {
						page.ShowBlockList(t.X, t.Y)
					}
				} else {
					page.gui.RemoveWidget(page.actionList)
					page.actionList.Free()
					page.actionList = nil
				}
			}
		case *sdl.KeyDownEvent:
			if page.gui.KeydownEvent(t) {
				break
			}
			page.KeyCommon(t)
		case *sdl.TextInputEvent:
			if page.gui.TextEvent(t) {
				break
			}
			bytes := []byte{}
			for _, char := range t.Text {
				if char != 0 {
					bytes = append(bytes, char)
				} else {
					break
				}
			}
			if len(bytes) != 0 {
				str := string(bytes)
				fmt.Println("Key was pressed: ", str)
			}
		}
	}
}

func (page *GamePage) KeyCommon(t *sdl.KeyDownEvent) {
	state := page.state
	switch int(t.Keysym.Sym) {
	case sdl.K_s:
		fmt.Println("s pressed")
		state.SendSay("message")
	case sdl.K_RETURN:
		fmt.Println("enter")
	case sdl.K_ESCAPE:
		page.running = false
	}
}
