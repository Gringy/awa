package main

import (
	"fmt"
	"time"

	"github.com/veandco/go-sdl2/sdl"

	"bitbucket.org/gringy/awa/cmd/awa_sdl/gui"
	"bitbucket.org/gringy/awa/common"
)

type GamePage struct {
	game *Game

	running   bool
	conn      *Connection
	state     *State
	lastTime  time.Time
	mouse     common.Point
	realMouse common.Point

	idForAction    uint32
	pointForAction sdl.Point
	actionList     gui.Widget
	paramsMsg      *gui.Label
	debugMsg       *gui.Label
	debugMsg2      *gui.Label
	holderWindows  []*HolderWindow
	draggedItem    *sdl.Rect
	chat           *Chat

	hpBar *gui.Bar
	epBar *gui.Bar
	turn  *gui.Bar

	gui *gui.GuiManager

	input  chan []byte
	output chan []byte
}

type HolderWindow struct {
	window *gui.Window
	items  []*gui.ItemWidget
	holder *Holder
}

func (hw *HolderWindow) Close(page *GamePage) {
	page.gui.RemoveWidget(hw.window)
}

func (hw *HolderWindow) Update(page *GamePage) {
	iconSize := int32(page.game.config.IconSize)
	for y := 0; y < hw.holder.size.Y; y++ {
		for x := 0; x < hw.holder.size.X; x++ {
			img := hw.items[y*hw.holder.size.X+x]
			item := hw.holder.items[y*hw.holder.size.X+x]
			itemDesc := page.state.items[item.tp]
			if itemDesc == nil {
				page.state.AddItemRequest(item.tp)
				item = Item{}
			}
			if item.tp != 0 {
				drawX := int32((itemDesc.DrawIndex % 10) * uint16(iconSize))
				drawY := int32((itemDesc.DrawIndex / 10) * uint16(iconSize))
				img.SetImageRect(&sdl.Rect{drawX, drawY, iconSize, iconSize}).SetImage(page.game.objTiles)
				img.UpdateLabels(int(item.count), int(item.quality))
			} else {
				img.SetImage(nil).SetImageRect(&sdl.Rect{0, 0, iconSize, iconSize})
			}
		}
	}
}

func (page *GamePage) AddHolderWindow(hw *HolderWindow) {
	page.holderWindows = append(page.holderWindows, hw)
}

func (page *GamePage) RemoveHolderWindow(hw *HolderWindow) {
	for i, w := range page.holderWindows {
		if w == hw {
			page.holderWindows = append(page.holderWindows[:i], page.holderWindows[i+1:]...)
			return
		}
	}
}

func (page *GamePage) Init(game *Game) {
	page.game = game
	page.lastTime = time.Now()

	page.state = NewState(page)
	page.input = make(chan []byte, 1)
	page.output = make(chan []byte, 10)
	page.conn = NewConnection(page, game.config.Url, game.config.Login, game.config.Password)
	go page.conn.Connect()

	page.running = true

	page.gui = gui.NewGuiManager(game.surface, game.font)

	sdl.StartTextInput()
	leftTop := gui.NewVBox(func(sdl.Point) sdl.Point {
		return sdl.Point{0, 0}
	}, 0xFFAAAAAA)
	page.paramsMsg = gui.NewLabel(page.game.font, "c")
	page.debugMsg = gui.NewLabel(page.game.font, "a")
	page.debugMsg2 = gui.NewLabel(page.game.font, "b")
	page.turn = gui.NewBar(page.game.font, 150).SetColors(0xFF00FF00, 0xFF225522)
	page.hpBar = gui.NewBar(page.game.font, 150).SetColors(0xFFFF0000, 0xFF552222)
	page.epBar = gui.NewBar(page.game.font, 150).SetColors(0xFF0000FF, 0xFF222255)
	leftTop.AddWidget(page.hpBar)
	leftTop.AddWidget(page.epBar)
	leftTop.AddWidget(page.turn)
	leftTop.AddWidget(page.paramsMsg)
	leftTop.AddWidget(page.debugMsg)
	leftTop.AddWidget(page.debugMsg2)

	page.gui.AddWidget(leftTop)

	page.chat = NewChat(page)
}

func (page *GamePage) UpdateParams() {
	params := page.state.playerParams
	page.paramsMsg.SetText(fmt.Sprintf("STR:%d DEX:%d WIS:%d", params.STR, params.DEX, params.WIS))
	page.hpBar.Val = int(params.HP)
	page.hpBar.Of = int(params.MaxHP())
	page.hpBar.UpdateText()
	page.epBar.Val = int(params.EP)
	page.epBar.Of = int(params.MaxEP())
	page.epBar.UpdateText()
}

func (page *GamePage) MillisOfTurn() int {
	return int(time.Now().Sub(page.lastTime).Seconds() * 1000)
}

func (page *GamePage) Tick() {
	page.turn.Val = page.MillisOfTurn()
	page.turn.Of = int(page.state.tickTime)
	page.turn.UpdateText()

	page.state.TryToReceive()
	page.Events()
	page.Draw()
	page.state.SendItemRequest()
	if !page.running {
		page.game.PopPage()
	}
}

func (page *GamePage) GetDebugMessage() string {
	x, y := page.state.RelativeToAbsolute(page.mouse.X, page.mouse.Y)
	tp := page.state.GetTypeAt(x, y)
	desc := page.state.GetBlockDesc(tp)
	name := ""
	if desc != nil {
		name = desc.Name
	}
	return fmt.Sprintf("FPS: %f Mouse: (%d,%d)  Type:%s(#%d)", page.game.fps, page.mouse.X, page.mouse.Y, name, tp)
}
