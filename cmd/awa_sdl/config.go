package main

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	Url      string `json:"url"`
	Login    string `json:"login"`
	Password string `json:"password"`
	Res_w    int    `json:"res_w"`
	Res_h    int    `json:"res_h"`
	IconSize int    `json:"icon_size"`
}

func ReadConfig() (*Config, error) {
	bytes, err := ioutil.ReadFile("config.json")
	if err != nil {
		return nil, err
	}
	var conf Config
	err = json.Unmarshal(bytes, &conf)
	if err != nil {
		return nil, err
	}
	return &conf, nil
}
