package main

import (
	"bitbucket.org/gringy/awa/cmd/awa_sdl/gui"
	"github.com/veandco/go-sdl2/sdl"
)

type Chat struct {
	page         *GamePage
	input        *gui.TextInput
	box          *gui.VBox
	labels       []*gui.Label
	messageCount int
}

func (chat *Chat) Slice() {
	for i := 0; i < len(chat.labels)-1; i++ {
		text := chat.labels[i+1].Text()
		color := chat.labels[i+1].Color()
		chat.labels[i].SetText(text)
		chat.labels[i].SetColor(color)
	}
}

func (chat *Chat) AddMessage(msg string) {
	chat.Slice()
	if len(msg) == 0 {
		msg = " "
	}
	chat.labels[len(chat.labels)-1].SetText(msg)
	chat.labels[len(chat.labels)-1].SetColor(sdl.Color{A: 0xFF, G: 0xFF})
}

func (chat *Chat) AddSystemMessage(msg string) {
	chat.Slice()
	if len(msg) == 0 {
		msg = " "
	}
	chat.labels[len(chat.labels)-1].SetText(msg)
	chat.labels[len(chat.labels)-1].SetColor(sdl.Color{A: 0xFF, R: 0xFF})
}

func NewChat(page *GamePage) *Chat {
	input := gui.NewTextInput(page.game.font, page.gui)
	input.SetSubmitFunc(func() {
		page.state.SendSay(input.GetText())
		input.SetText("")
	})
	var box *gui.VBox
	box = gui.NewVBox(func(scr sdl.Point) sdl.Point {
		size := box.Size()
		return sdl.Point{0, scr.Y - size.Y}
	}, 0xFF223322)
	ret := &Chat{
		input: input,
		box:   box,
	}
	ret.labels = make([]*gui.Label, 10)
	for i := range ret.labels {
		ret.labels[i] = gui.NewLabel(page.game.font, " ")
		box.AddWidget(ret.labels[i])
	}
	box.AddWidget(input)
	page.gui.AddWidget(box)
	return ret
}
