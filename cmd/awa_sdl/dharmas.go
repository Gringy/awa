package main

import (
	"fmt"
	"strconv"
	"time"

	"bitbucket.org/gringy/awa/common"
	"bitbucket.org/gringy/awa/common/binary"
)

func (state *State) ReadDharma(reader *binary.Reader) {
	state.game.lastTime = time.Now()
	needToUpdateItems := false

	defer func() {
		some := recover()
		if some != nil {
			err := some.(error)
			fmt.Println("(*State).ReadDharma: ", err)
		}
	}()

	count, err := reader.ReadShort()
	if err != nil {
		fmt.Println(err)
		return
	}
	for i := 0; i < int(count); i++ {
		tp, err := reader.ReadByte()
		if err != nil {
			fmt.Println(err)
			return
		}
		switch tp {
		case 0x00:
			// move dharma
			id := reader.MustInt()
			x := reader.MustByte()
			y := reader.MustByte()
			state.MoveObject(id, x, y)
		case 0x01:
			//say dharma
			id := reader.MustInt()
			msg := reader.MustString()
			if id != 0 {
				state.game.chat.AddMessage("from #" + strconv.Itoa(int(id)) + ": " + msg)
			} else {
				state.game.chat.AddSystemMessage(msg)
			}
		case 0x02:
			//add dharma
			chunkID := reader.MustInt()
			obj := ReadObject(reader)
			for _, chunk := range state.chunks {
				if chunk != nil && chunk.id == chunkID {
					chunk.objects = append(chunk.objects, obj)
				}
			}
		case 0x03:
			//del dharma
			chunkID := reader.MustInt()
			gameID := reader.MustInt()
			for _, chunk := range state.chunks {
				if chunk != nil && chunk.id == chunkID {
					for i, obj := range chunk.objects {
						if obj.id == gameID {
							chunk.objects = append(chunk.objects[:i], chunk.objects[i+1:]...)
							break
						}
					}
				}
			}
		case 0x04:
			//change dharma
			chunkID := reader.MustInt()
			x := reader.MustByte()
			y := reader.MustByte()
			tp := reader.MustShort()
			for _, chunk := range state.chunks {
				if chunk != nil && chunk.id == chunkID {
					chunk.cells.SetTypeAt(int(x), int(y), common.BlockType(tp))
				}
			}
		case 0x05:
			// block description
			tp := reader.MustShort()
			index := reader.MustByte()
			pass := reader.MustByte()
			name := reader.MustString()
			passable := false
			if pass != 0 {
				passable = true
			}
			block := BlockType{
				Id:        tp,
				Name:      name,
				DrawIndex: index,
				Passable:  bool(passable),
			}
			fmt.Println("Readed: ", block)
			state.blocks[tp] = &block
			state.requestSendBlocker = false
		case 0x06:
			// holder dharma
			id := reader.MustInt()
			xsize := reader.MustByte()
			ysize := reader.MustByte()
			items := make([]Item, xsize*ysize)
			for i := 0; i < int(xsize)*int(ysize); i++ {
				item, err := ReadItem(reader)
				if err != nil {
					fmt.Println(err)
					return
				}
				items[i] = item
			}
			holder := Holder{
				gameID: id,
				size:   common.Point{int(xsize), int(ysize)},
				items:  items,
			}
			state.AddHolder(&holder)
		case 0x07:
			// holder change dharma
			gameID := reader.MustInt()
			x := reader.MustByte()
			y := reader.MustByte()
			item, err := ReadItem(reader)
			if err != nil {
				fmt.Println(err)
				return
			}
			for _, holder := range state.holders {
				if holder.gameID == gameID {
					if holder.size.X > int(x) && holder.size.Y > int(y) {
						holder.items[int(y)*holder.size.X+int(x)] = item
					}
				}
			}
			for _, hw := range state.game.holderWindows {
				if hw.holder.gameID == gameID {
					hw.Update(state.game)
				}
			}
		case 0x08:
			// holder close dharma
			gameID := reader.MustInt()
			for _, hw := range state.game.holderWindows {
				if hw.holder.gameID == gameID {
					hw.Close(state.game)
					state.game.RemoveHolderWindow(hw)
				}
			}
		case 0x09:
			// item description dharma
			tp := reader.MustInt()
			grp := reader.MustInt()
			name := reader.MustString()
			stack := reader.MustByte()
			drawIndex := reader.MustShort()
			state.items[tp] = &ItemType{
				Type:      tp,
				Group:     grp,
				Name:      name,
				Stack:     stack,
				DrawIndex: drawIndex,
			}
			fmt.Println("Readed: ", state.items[tp])
			needToUpdateItems = true
		case 0x0A:
			// params change dharma
			p := ReadParams(reader)
			state.playerParams = p
			state.game.UpdateParams()
		case 0x0B:
			// object type change dharma
			objID := reader.MustInt()
			tp := reader.MustShort()
			for _, ch := range state.chunks {
				if ch != nil {
					for _, obj := range ch.objects {
						if obj.id == objID {
							obj.tp = tp
						}
					}
				}
			}
		default:
			fmt.Println("Unknown dharma #", tp)
			return
		}
	}
	if needToUpdateItems {
		for _, win := range state.game.holderWindows {
			win.Update(state.game)
		}
	}
}
