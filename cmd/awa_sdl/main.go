// Command awa_sdl is sdl implementation of awa client
package main

import (
	"os"
	"time"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

type Page interface {
	Tick()
}

type Game struct {
	pageStack []Page
	window    *sdl.Window
	config    *Config

	surface  *sdl.Surface
	gndTiles *sdl.Surface
	hrsTiles *sdl.Surface
	objTiles *sdl.Surface
	font     *ttf.Font

	fps float64
}

func (game *Game) LoadConfig() {
	config, err := ReadConfig()
	if err != nil {
		panic(err)
	}
	game.config = config
}

func (game *Game) LoadSDL() {
	sdl.Init(sdl.INIT_EVERYTHING)
	if err := ttf.Init(); err != nil {
		panic(err)
	}
	if font, err := ttf.OpenFont("font.ttf", 12); err != nil {
		panic(err)
	} else {
		game.font = font
	}

	window, err := sdl.CreateWindow("Awa", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		game.config.Res_w, game.config.Res_h, sdl.WINDOW_SHOWN)
	if err != nil {
		panic(err)
	}
	game.window = window

	surface, err := window.GetSurface()
	if err != nil {
		panic(err)
	}
	game.surface = surface
}

func (game *Game) LoadImages() {
	image, err := img.Load("gnd.png")
	if err != nil {
		panic(err)
	}
	game.gndTiles = image

	image, err = img.Load("hrs.png")
	if err != nil {
		panic(err)
	}
	game.hrsTiles = image

	image, err = img.Load("icon.png")
	if err != nil {
		panic(err)
	}
	game.objTiles = image
}

func (game *Game) Run() int {
	for len(game.pageStack) != 0 {
		now := time.Now()
		game.pageStack[len(game.pageStack)-1].Tick()
		sdl.Delay(10)
		delay := time.Now().Sub(now).Seconds()
		game.fps = 1 / delay
	}
	sdl.Quit()
	return 0
}

func (game *Game) PopPage() {
	if len(game.pageStack) != 0 {
		game.pageStack = game.pageStack[:len(game.pageStack)-1]
	}
}

func NewGame() (*Game, error) {
	game := &Game{}

	game.LoadConfig()
	game.LoadSDL()
	game.LoadImages()

	game.pageStack = append(game.pageStack, NewLogin(game))
	return game, nil
}

func main() {
	game, err := NewGame()
	if err != nil {
		panic(err)
	}
	os.Exit(game.Run())
}
