package main

import (
	"fmt"

	"bitbucket.org/gringy/awa/cmd/awa_sdl/gui"
	"bitbucket.org/gringy/awa/common"
	"github.com/veandco/go-sdl2/sdl"
)

type LoginPage struct {
	game        *Game
	running     bool
	realMouse   common.Point
	gui         *gui.GuiManager
	loginWindow *gui.Window

	loginButton *gui.Button

	loginInput *gui.TextInput
	passInput  *gui.TextInput
}

func (page *LoginPage) Tick() {
	page.Events()
	page.game.surface.FillRect(&sdl.Rect{0, 0, page.game.surface.W, page.game.surface.H}, 0xFF000000)
	page.gui.Draw()
	page.game.window.UpdateSurface()
	if !page.running {
		page.game.PopPage()
	}
}

func (page *LoginPage) Events() {
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			page.running = false
		case *sdl.MouseMotionEvent:
			page.realMouse.X = int(t.X)
			page.realMouse.Y = int(t.Y)
			if page.gui.MouseMove(t) {
				continue
			}
		case *sdl.MouseButtonEvent:
			if page.gui.MouseEvent(t) {
				break
			}
		case *sdl.KeyDownEvent:
			if page.gui.KeydownEvent(t) {
				break
			}
			page.KeyCommon(t)
		case *sdl.TextInputEvent:
			if page.gui.TextEvent(t) {
				break
			}
		}
	}
}

func (page *LoginPage) KeyCommon(t *sdl.KeyDownEvent) {
	switch int(t.Keysym.Sym) {
	case sdl.K_s:
		fmt.Println("s pressed")
		//state.SendSay("message")
	case sdl.K_RETURN:
		fmt.Println("enter")
	case sdl.K_ESCAPE:
		page.running = false
	}
}

func NewLogin(game *Game) *LoginPage {
	page := &LoginPage{
		game:    game,
		gui:     gui.NewGuiManager(game.surface, game.font),
		running: true,
	}
	page.loginInput = gui.NewTextInput(game.font, page.gui)
	page.loginInput.SetText(game.config.Login)
	page.passInput = gui.NewTextInput(game.font, page.gui)
	page.passInput.SetText(game.config.Password)
	page.loginButton = gui.NewButton(game.font, "Play", func() {
		game.config.Login = page.loginInput.GetText()
		game.config.Password = page.passInput.GetText()
		page := &GamePage{}
		page.Init(game)
		game.pageStack = append(game.pageStack, page)
	})
	widgets := []gui.Widget{page.loginInput, page.passInput, page.loginButton}
	vBox := gui.NewFilledVBox(nil, 0xFF222222, widgets)
	page.loginWindow = gui.NewWindow(page.gui, gui.NewButton(game.font, "X", func() {
		page.running = false
	}), vBox)
	windowSize := page.loginWindow.Size()
	page.loginWindow.SetPosition(sdl.Point{page.game.surface.W/2 - windowSize.X/2, page.game.surface.H/2 - windowSize.Y/2})
	page.gui.AddWidget(page.loginWindow)
	return page
}
