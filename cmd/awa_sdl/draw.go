package main

import (
	"bitbucket.org/gringy/awa/common/path"
	"github.com/veandco/go-sdl2/sdl"
)

func (page *GamePage) DrawSprite(sp *sdl.Surface, fx, fy, tx, ty int32) {
	//src, dst
	sp.Blit(&sdl.Rect{fx * 32, fy * 32, 32, 32}, page.game.surface, &sdl.Rect{tx, ty, 32, 32})
}

func (page *GamePage) Draw() {
	game := page.game
	w, h := game.surface.W, game.surface.H
	size := int32(32)
	state := page.state
	// clear screen
	game.surface.FillRect(&sdl.Rect{0, 0, w, h}, 0xFF000000)

	if !state.IsInit() {
		game.window.UpdateSurface()
		return
	}

	player := state.GetObjectById(state.playerGameID)
	if player == nil {
		return
	}
	playerChunkX, playerChunkY := state.GetPlayerChunkCoords()
	playerX := int32(playerChunkX*32) + int32(player.x)
	playerY := int32(playerChunkY*32) + int32(player.y)
	// walls
	for x := 0; x < 96; x++ {
		for y := 0; y < 96; y++ {
			tp := state.GetTypeAt(x, y)
			blockDesc := state.GetBlockDesc(tp)
			if blockDesc != nil {
				drawX := w/2 + (int32(x)-playerX)*size
				drawY := h/2 - (int32(y)-playerY)*size
				page.DrawSprite(game.gndTiles, int32(blockDesc.DrawIndex%8), int32(blockDesc.DrawIndex/8), drawX, drawY)
			}
		}
	}
	// draw area
	for x := -5; x <= 5; x++ {
		for y := -5; y <= 5; y++ {
			val := state.area.GetAt(x, y)
			if val != path.UNCHECKED && val != path.WALL {
				drawX := w/2 + (int32(x))*size
				drawY := h/2 - (int32(y))*size
				page.DrawSprite(game.gndTiles, 7, 7, drawX, drawY)
			}
		}
	}
	// objects
	for i := 0; i < 9; i++ {
		if state.chunks[i] == nil {
			continue
		}
		for _, object := range state.chunks[i].objects {
			objX := i%3*32 + int(object.x)
			objY := i/3*32 + int(object.y)
			drawX := w/2 + (int32(objX)-playerX)*size
			drawY := h/2 - (int32(objY)-playerY)*size
			drawIndex := uint8(0)
			found := false
			for _, di := range object.indexes {
				if object.tp == di.tp {
					found = true
					drawIndex = di.di
					break
				}
			}
			if found {
				page.DrawSprite(game.hrsTiles, int32(drawIndex%8), int32(drawIndex/8), drawX, drawY)
			}

		}
	}

	// gui
	page.gui.Draw()

	if page.draggedItem != nil {
		game.objTiles.Blit(page.draggedItem, game.surface, &sdl.Rect{int32(page.realMouse.X), int32(page.realMouse.Y), page.draggedItem.W, page.draggedItem.H})
	}

	game.window.UpdateSurface()

	state.SendBlockRequest()
}
