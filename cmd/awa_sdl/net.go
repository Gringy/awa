package main

import (
	"fmt"

	"bitbucket.org/gringy/awa/common/binary"

	"github.com/gorilla/websocket"
)

type Connection struct {
	url, login, password string

	game *GamePage
}

type Message struct{}

func NewConnection(game *GamePage, url, login, password string) *Connection {
	conn := new(Connection)
	conn.game = game
	conn.url, conn.login, conn.password = url, login, password
	return conn
}

func FormLoginMessage(login, password string) []byte {
	writer := binary.NewWriter()
	writer.WriteString(login)
	writer.WriteString(password)
	return writer.GetBytes()
}

// Blocking
func (conn *Connection) Connect() {
	c, _, err := websocket.DefaultDialer.Dial(conn.url, nil)
	if err != nil {
		fmt.Print(err)
		return
	}
	defer c.Close()

	c.WriteMessage(websocket.BinaryMessage, FormLoginMessage(conn.login, conn.password))

	go func() {
		for {
			_, bytes, err := c.ReadMessage()
			if err != nil {
				fmt.Println(err)
				return
			}
			conn.game.input <- bytes
		}
	}()

	for {
		err := c.WriteMessage(websocket.BinaryMessage, <-conn.game.output)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}
