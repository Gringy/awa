package gui

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

type ItemWidget struct {
	count, quality       int
	preCount, preQuality *sdl.Surface
	font                 *ttf.Font
	src                  *sdl.Surface
	rect                 *sdl.Rect
	clickedFunc          func()
	textColor            sdl.Color
}

func (img *ItemWidget) UpdateLabels(count, quality int) {
	if count != img.count || img.preCount == nil {
		img.count = count
		if img.preCount != nil {
			img.preCount.Free()
		}
		result, err := img.font.RenderUTF8_Solid(fmt.Sprint(count), img.textColor)
		if err != nil {
			panic(err)
		}
		img.preCount = result
	}
	if quality != img.quality || img.preQuality == nil {
		img.quality = quality
		if img.preQuality != nil {
			img.preQuality.Free()
		}
		result, err := img.font.RenderUTF8_Solid(fmt.Sprint(quality), img.textColor)
		if err != nil {
			panic(err)
		}
		img.preQuality = result
	}
}

func (img *ItemWidget) PreferredPosition(sdl.Point) sdl.Point {
	return sdl.Point{}
}

func (img *ItemWidget) Size() sdl.Point {
	return sdl.Point{img.rect.W + 4, img.rect.H + 4}
}

func (img *ItemWidget) Draw(context *Context) {
	if img.src == nil {
		return
	}
	context.DrawImage(&sdl.Rect{2, 2, img.rect.W, img.rect.H}, img.rect, img.src)
	if img.preCount == nil || img.preQuality == nil {
		img.UpdateLabels(img.count, img.quality)
	}
	context.DrawImage(&sdl.Rect{img.rect.W - img.preCount.W, 2, img.preCount.W, img.preCount.H}, nil, img.preCount)
	context.DrawImage(&sdl.Rect{img.rect.W - img.preQuality.W, img.rect.H - img.preQuality.H, img.preQuality.W, img.preQuality.H}, nil, img.preQuality)
}

func (img *ItemWidget) Visible() bool {
	return true
}

func (img *ItemWidget) OnMouseEvent(p sdl.Point, tp MouseEventType) bool {
	if img.clickedFunc != nil && tp == MOUSEUP {
		img.clickedFunc()
	}
	return true
}

func (img *ItemWidget) Free() {
	if img.preCount != nil {
		img.preCount.Free()
		img.preCount = nil
	}
	if img.preQuality != nil {
		img.preQuality.Free()
		img.preQuality = nil
	}
}

func NewItemWidget(font *ttf.Font, textColor sdl.Color, clickedFunc func()) *ItemWidget {
	return &ItemWidget{
		font:        font,
		clickedFunc: clickedFunc,
		textColor:   textColor,
	}

}

func (img *ItemWidget) SetClickedFunc(fn func()) *ItemWidget {
	img.clickedFunc = fn
	return img
}

func (img *ItemWidget) SetImage(src *sdl.Surface) *ItemWidget {
	img.src = src
	return img
}

func (img *ItemWidget) SetImageRect(rect *sdl.Rect) *ItemWidget {
	img.rect = rect
	return img
}

func (img *ItemWidget) ImageRect() *sdl.Rect {
	return img.rect
}
