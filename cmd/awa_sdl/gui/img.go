package gui

import "github.com/veandco/go-sdl2/sdl"

type Img struct {
	src         *sdl.Surface
	rect        *sdl.Rect
	clickedFunc func()
}

func (img *Img) PreferredPosition(sdl.Point) sdl.Point {
	return sdl.Point{}
}

func (img *Img) Size() sdl.Point {
	return sdl.Point{img.rect.W + 4, img.rect.H + 4}
}

func (img *Img) Draw(context *Context) {
	if img.src == nil {
		return
	}
	context.DrawImage(&sdl.Rect{2, 2, img.rect.W, img.rect.H}, img.rect, img.src)
}

func (img *Img) Visible() bool {
	return true
}

func (img *Img) OnMouseEvent(p sdl.Point, tp MouseEventType) bool {
	if img.clickedFunc != nil && tp == MOUSEUP {
		img.clickedFunc()
	}
	return true
}

func (img *Img) Free() {}

func (img *Img) SetImage(src *sdl.Surface) *Img {
	img.src = src
	return img
}

func (img *Img) SetImageRect(rect *sdl.Rect) *Img {
	img.rect = rect
	return img
}
