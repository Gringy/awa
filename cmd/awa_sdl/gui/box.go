package gui

import "github.com/veandco/go-sdl2/sdl"

// Struct VBox
type VBox struct {
	placeFunc func(sdl.Point) sdl.Point
	widgets   []Widget
	color     uint32
}

func (vbox *VBox) AddWidget(widget Widget) {
	vbox.widgets = append(vbox.widgets, widget)
}

func (vbox *VBox) PreferredPosition(pos sdl.Point) sdl.Point {
	if vbox.placeFunc == nil {
		return sdl.Point{}
	} else {
		return vbox.placeFunc(pos)
	}
}

func (vbox *VBox) Size() sdl.Point {
	ret := sdl.Point{}
	for _, widget := range vbox.widgets {
		size := widget.Size()
		ret.Y += size.Y
		if size.X > ret.X {
			ret.X = size.X
		}
	}
	return ret
}

func (vbox *VBox) Draw(context *Context) {
	current := sdl.Point{}
	fullSize := vbox.Size()
	if vbox.color != 0 {
		context.FillColor(sdl.Rect{0, 0, fullSize.X, fullSize.Y}, vbox.color)
	}
	for _, widget := range vbox.widgets {
		size := widget.Size()
		sub := context.SubContext(sdl.Rect{current.X, current.Y, fullSize.X, size.Y})
		widget.Draw(sub)
		current.Y += size.Y
	}
}

func (vbox *VBox) Visible() bool {
	return true
}

func (vbox *VBox) OnMouseEvent(click sdl.Point, tp MouseEventType) bool {
	current := sdl.Point{}
	for _, widget := range vbox.widgets {
		size := widget.Size()
		if click.X >= current.X && click.X < current.X+size.X &&
			click.Y >= current.Y && click.Y < current.Y+size.Y {
			return widget.OnMouseEvent(sdl.Point{click.X - current.X, click.Y - current.Y}, tp)
		}
		current.Y += size.Y
	}
	return false
}

func (vbox *VBox) Free() {
	for _, widget := range vbox.widgets {
		widget.Free()
	}
}

func NewVBox(placeFunc func(sdl.Point) sdl.Point, color uint32) *VBox {
	return &VBox{
		placeFunc: placeFunc,
		color:     color,
	}
}

func NewFilledVBox(placeFunc func(sdl.Point) sdl.Point, color uint32, widgets []Widget) *VBox {
	return &VBox{
		placeFunc: placeFunc,
		color:     color,
		widgets:   widgets,
	}
}

// Struct HBox
type HBox struct {
	placeFunc func(sdl.Point) sdl.Point
	widgets   []Widget
}

func (hbox *HBox) AddWidget(widget Widget) {
	hbox.widgets = append(hbox.widgets, widget)
}

func (hbox *HBox) PreferredPosition(pos sdl.Point) sdl.Point {
	if hbox.placeFunc == nil {
		return sdl.Point{}
	} else {
		return hbox.placeFunc(pos)
	}
}

func (hbox *HBox) Size() sdl.Point {
	ret := sdl.Point{}
	for _, widget := range hbox.widgets {
		size := widget.Size()
		ret.X += size.X
		if size.Y > ret.Y {
			ret.Y = size.Y
		}
	}
	return ret
}

func (hbox *HBox) Draw(context *Context) {
	current := sdl.Point{}
	for _, widget := range hbox.widgets {
		size := widget.Size()
		sub := context.SubContext(sdl.Rect{current.X, current.Y, size.X, size.Y})
		widget.Draw(sub)
		current.X += size.X
	}
}

func (hbox *HBox) Visible() bool {
	return true
}

func (hbox *HBox) OnMouseEvent(click sdl.Point, tp MouseEventType) bool {
	current := sdl.Point{}
	for _, widget := range hbox.widgets {
		size := widget.Size()
		if click.X >= current.X && click.X < current.X+size.X &&
			click.Y >= current.Y && click.Y < current.Y+size.Y {
			return widget.OnMouseEvent(sdl.Point{click.X - current.X, click.Y - current.Y}, tp)

		}
		current.X += size.X
	}
	return false
}

func (hbox *HBox) Free() {
	for _, widget := range hbox.widgets {
		widget.Free()
	}
}

func NewHBox(placeFunc func(sdl.Point) sdl.Point) *HBox {
	return &HBox{
		placeFunc: placeFunc,
	}
}

func NewFilledHBox(placeFunc func(sdl.Point) sdl.Point, widgets []Widget) *HBox {
	return &HBox{
		placeFunc: placeFunc,
		widgets:   widgets,
	}
}
