package gui

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

// Struct Button
type Button struct {
	font        *ttf.Font
	rendered    *sdl.Surface
	text        string
	clickedFunc func()
}

func (button *Button) UpdateText() {
	solid, err := button.font.RenderUTF8_Solid(button.text, sdl.Color{A: 0xFF})
	if err != nil {
		fmt.Println(err)
	}
	button.rendered = solid
}

func (button *Button) Size() sdl.Point {
	if button.rendered == nil {
		button.UpdateText()
	}
	return sdl.Point{button.rendered.W + 2, button.rendered.H + 2}
}

func (button *Button) Draw(context *Context) {
	if button.rendered == nil {
		button.UpdateText()
	}
	// fill all the area of button
	context.FillColor(sdl.Rect{0, 0, context.area.W, context.area.H}, 0xffffffff)
	context.DrawImage(&sdl.Rect{1, 1, button.rendered.W, button.rendered.H}, nil, button.rendered)
}

func (button *Button) Visible() bool {
	return true
}

func (button *Button) PreferredPosition(sdl.Point) sdl.Point {
	return sdl.Point{}
}

func (button *Button) OnMouseEvent(p sdl.Point, tp MouseEventType) bool {
	if button.clickedFunc != nil && tp == MOUSEUP {
		button.clickedFunc()
	}
	return true
}

func (button *Button) Free() {
	if button.rendered != nil {
		button.rendered.Free()
		button.rendered = nil
	}
}

func NewButton(font *ttf.Font, text string, clicked func()) *Button {
	return &Button{
		font:        font,
		text:        text,
		clickedFunc: clicked,
	}
}
