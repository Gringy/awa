package gui

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

type Bar struct {
	Val, Of               int
	width                 int32
	font                  *ttf.Font
	rendered              *sdl.Surface
	fillColor, spaceColor uint32
}

func (bar *Bar) UpdateText() {
	text := fmt.Sprintf("%d/%d", bar.Val, bar.Of)
	solid, err := bar.font.RenderUTF8_Solid(text, sdl.Color{A: 0xFF})
	if err != nil {
		fmt.Println(err)
	}
	if bar.rendered != nil {
		bar.rendered.Free()
		bar.rendered = nil
	}
	bar.rendered = solid
}

func (bar *Bar) PreferredPosition(sdl.Point) sdl.Point {
	return sdl.Point{}
}

func (bar *Bar) Size() sdl.Point {
	if bar.rendered == nil {
		bar.UpdateText()
	}
	return sdl.Point{bar.width, bar.rendered.H + 2}
}

func (bar *Bar) Draw(context *Context) {
	if bar.rendered == nil {
		bar.UpdateText()
	}
	var of float32
	if bar.Of == 0 {
		of = 1
	} else {
		of = float32(bar.Val) / float32(bar.Of)
		if of < 0 {
			of = 0
		}
		if of > 1 {
			of = 1
		}
	}
	fillWidth := int32(float32(bar.width) * of)
	spaceWidth := bar.width - fillWidth
	context.FillColor(sdl.Rect{0, 0, fillWidth, bar.rendered.H + 2}, bar.fillColor)
	context.FillColor(sdl.Rect{fillWidth, 0, spaceWidth, bar.rendered.H + 2}, bar.spaceColor)
	context.DrawImage(&sdl.Rect{(bar.width - bar.rendered.W) / 2, 1, bar.rendered.W, bar.rendered.H}, nil, bar.rendered)
}

func (bar *Bar) Visible() bool {
	return true
}

func (bar *Bar) OnMouseEvent(sdl.Point, MouseEventType) bool {
	return true
}

func (bar *Bar) Free() {
	if bar.rendered != nil {
		bar.rendered.Free()
		bar.rendered = nil
	}
}

func (bar *Bar) SetColors(fill, space uint32) *Bar {
	bar.fillColor = fill
	bar.spaceColor = space
	return bar
}

func (bar *Bar) SetValues(val, of int) *Bar {
	bar.Val = val
	bar.Of = of
	return bar
}

func NewBar(font *ttf.Font, width int32) *Bar {
	return &Bar{
		font:  font,
		width: width,
	}
}
