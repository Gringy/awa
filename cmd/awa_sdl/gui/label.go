package gui

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

// Struct Label
type Label struct {
	font     *ttf.Font
	rendered *sdl.Surface
	text     string
	color    sdl.Color
}

func (label *Label) UpdateText() {
	solid, err := label.font.RenderUTF8_Solid(label.text, label.color)
	if err != nil {
		fmt.Println(err)
	}
	label.Free()
	label.rendered = solid
}

func (label *Label) PreferredPosition(sdl.Point) sdl.Point {
	return sdl.Point{}
}

func (label *Label) Size() sdl.Point {
	if label.rendered == nil {
		label.UpdateText()
	}
	return sdl.Point{label.rendered.W + 2, label.rendered.H + 2}
}

func (label *Label) Draw(context *Context) {
	if label.rendered == nil {
		label.UpdateText()
	}
	context.DrawImage(&sdl.Rect{1, 1, label.rendered.W, label.rendered.H}, nil, label.rendered)
}

func (label *Label) Visible() bool {
	return true
}

func (label *Label) OnMouseEvent(sdl.Point, MouseEventType) bool {
	return true
}

func (label *Label) Free() {
	if label.rendered != nil {
		label.rendered.Free()
		label.rendered = nil
	}
}

func (label *Label) SetText(text string) *Label {
	label.text = text
	label.UpdateText()
	return label
}

func (label *Label) SetColor(color sdl.Color) *Label {
	label.color = color
	label.UpdateText()
	return label
}

func (label *Label) Text() string {
	return label.text
}

func (label *Label) Color() sdl.Color {
	return label.color
}

func NewLabel(font *ttf.Font, text string) *Label {
	return &Label{
		font:  font,
		text:  text,
		color: sdl.Color{A: 0xFF},
	}
}
