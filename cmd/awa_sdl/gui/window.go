package gui

import "github.com/veandco/go-sdl2/sdl"

type Window struct {
	pos         sdl.Point
	root        Widget
	closeButton *Button
	gui         *GuiManager

	inMoving  bool
	mustMouse sdl.Point
}

func (window *Window) PreferredPosition(sdl.Point) sdl.Point {
	return window.pos
}

func (window *Window) Size() sdl.Point {
	rootSize := window.root.Size()
	buttonSize := window.closeButton.Size()
	return sdl.Point{rootSize.X, rootSize.Y + buttonSize.Y}
}

func (window *Window) Draw(context *Context) {
	rootSize := window.root.Size()
	buttonSize := window.closeButton.Size()
	rootContext := context.SubContext(sdl.Rect{0, buttonSize.Y, rootSize.X, rootSize.Y})
	context.FillColor(sdl.Rect{0, 0, rootSize.X, buttonSize.Y}, 0xFFAAAAAA)
	window.root.Draw(rootContext)
	buttonContext := context.SubContext(sdl.Rect{rootSize.X - buttonSize.X, 0, buttonSize.X, buttonSize.Y})
	window.closeButton.Draw(buttonContext)
}

func (window *Window) Visible() bool {
	return true
}

func (window *Window) OnMouseEvent(p sdl.Point, tp MouseEventType) bool {
	rootSize := window.root.Size()
	buttonSize := window.closeButton.Size()
	// move area
	if window.inMoving || (!window.inMoving && p.X >= 0 && p.X < rootSize.X-buttonSize.X && p.Y >= 0 && p.Y < buttonSize.Y) {
		switch tp {
		case MOUSEDOWN:
			window.inMoving = true
			window.mustMouse = p
			window.gui.movePass = window
			return true
		case MOUSEMOVE_PASSED:
			if window.inMoving {
				window.pos.X += p.X - window.mustMouse.X
				window.pos.Y += p.Y - window.mustMouse.Y
			}
			return true
		case MOUSEUP:
			window.inMoving = false
			window.gui.movePass = nil
			return true
		case MOUSEMOVE:
			return true
		}
	}
	// button area
	if p.X >= rootSize.X-buttonSize.X && p.X < rootSize.X && p.Y >= 0 && p.Y < buttonSize.Y {
		return window.closeButton.OnMouseEvent(sdl.Point{p.X - rootSize.X, p.Y}, tp)

	}
	// root area
	if p.X >= 0 && p.X < rootSize.X && p.Y >= buttonSize.Y && p.Y < buttonSize.Y+rootSize.Y {
		return window.root.OnMouseEvent(sdl.Point{p.X, p.Y - buttonSize.Y}, tp)
	}
	return true
}

func (window *Window) Free() {
	window.root.Free()
	window.closeButton.Free()
}

func (window *Window) SetPosition(p sdl.Point) *Window {
	window.pos = p
	return window
}

func NewWindow(gui *GuiManager, closeButton *Button, root Widget) *Window {
	return &Window{
		gui:         gui,
		closeButton: closeButton,
		root:        root,
	}
}
