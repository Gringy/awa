package gui

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

type MouseEventType int

const (
	MOUSEUP MouseEventType = iota
	MOUSEDOWN
	MOUSEMOVE
	MOUSEMOVE_PASSED
)

type Widget interface {
	PreferredPosition(sdl.Point) sdl.Point
	Size() sdl.Point
	Draw(*Context)
	Visible() bool
	OnMouseEvent(sdl.Point, MouseEventType) bool
	Free()
}

type Key uint32

const (
	KEY_ENTER Key = iota
	KEY_BACKSPACE
	KEY_DELETE
	KEY_UP
	KEY_DOWN
	KEY_LEFT
	KEY_RIGHT
	KEY_ESCAPE
)

type TextEditor interface {
	OnRune(r rune)
	OnKeyboard(k Key)
}

type Context struct {
	surface *sdl.Surface
	area    sdl.Rect
}

func (context *Context) SubContext(area sdl.Rect) *Context {
	newContext := *context

	newContext.area.X += area.X
	newContext.area.Y += area.Y
	newContext.area.W = area.W
	newContext.area.H = area.H
	if newContext.area.X+newContext.area.W > context.area.X+context.area.W ||
		newContext.area.Y+newContext.area.H > context.area.Y+context.area.H {
		fmt.Println("(*Context).SubContext: request area is out of context area")
		return nil
	}

	return &newContext
}

func (context *Context) FillColor(area sdl.Rect, color uint32) {
	area.X += context.area.X
	area.Y += context.area.Y
	if area.X+area.W > context.area.X+context.area.W ||
		area.Y+area.H > context.area.Y+context.area.H {
		fmt.Println("(*Context).FillColor: request area is out of context area")
		return
	}
	context.surface.FillRect(&area, color)
}

func (context *Context) DrawImage(dst, src *sdl.Rect, img *sdl.Surface) {
	dst.X += context.area.X
	dst.Y += context.area.Y
	if dst.X+dst.W > context.area.X+context.area.W ||
		dst.Y+dst.H > context.area.Y+context.area.H {
		fmt.Println("(*Context).DrawImage: request area is out of context area")
		return
	}
	img.Blit(src, context.surface, dst)
}

type GuiManager struct {
	surface  *sdl.Surface
	font     *ttf.Font
	widgets  []Widget
	movePass Widget
	editor   TextEditor
}

func (gui *GuiManager) AddWidget(widget Widget) {
	gui.widgets = append(gui.widgets, widget)
}

func (gui *GuiManager) RemoveWidget(widget Widget) {
	for i, w := range gui.widgets {
		if w == widget {
			gui.widgets = append(gui.widgets[:i], gui.widgets[i+1:]...)
		}
	}
}

func (gui *GuiManager) Draw() {
	for _, widget := range gui.widgets {
		size := widget.Size()
		pos := widget.PreferredPosition(sdl.Point{gui.surface.W, gui.surface.H})
		context := Context{gui.surface, sdl.Rect{pos.X, pos.Y, size.X, size.Y}}
		widget.Draw(&context)
	}
}

func (gui *GuiManager) MouseEvent(ev *sdl.MouseButtonEvent) bool {
	for _, widget := range gui.widgets {
		size := widget.Size()
		pos := widget.PreferredPosition(sdl.Point{gui.surface.W, gui.surface.H})
		if ev.X >= pos.X && ev.X < pos.X+size.X &&
			ev.Y >= pos.Y && ev.Y < pos.Y+size.Y {
			switch ev.Type {
			case sdl.MOUSEBUTTONUP:
				return widget.OnMouseEvent(sdl.Point{ev.X - pos.X, ev.Y - pos.Y}, MOUSEUP)
			case sdl.MOUSEBUTTONDOWN:
				return widget.OnMouseEvent(sdl.Point{ev.X - pos.X, ev.Y - pos.Y}, MOUSEDOWN)
			}
		}
	}
	return false
}

func (gui *GuiManager) MouseMove(ev *sdl.MouseMotionEvent) bool {
	if gui.movePass == nil {
		for _, widget := range gui.widgets {
			size := widget.Size()
			pos := widget.PreferredPosition(sdl.Point{gui.surface.W, gui.surface.H})
			if ev.X >= pos.X && ev.X < pos.X+size.X &&
				ev.Y >= pos.Y && ev.Y < pos.Y+size.Y {
				return widget.OnMouseEvent(sdl.Point{ev.X - pos.X, ev.Y - pos.Y}, MOUSEMOVE)

			}
		}
	} else {
		for _, widget := range gui.widgets {
			if widget == gui.movePass {
				pos := widget.PreferredPosition(sdl.Point{gui.surface.W, gui.surface.H})
				return widget.OnMouseEvent(sdl.Point{ev.X - pos.X, ev.Y - pos.Y}, MOUSEMOVE_PASSED)
			}
		}
	}
	return false
}

func (gui *GuiManager) KeydownEvent(ev *sdl.KeyDownEvent) bool {
	if gui.editor == nil {
		return false
	}
	switch int(ev.Keysym.Sym) {
	case sdl.K_RETURN:
		gui.editor.OnKeyboard(KEY_ENTER)
	case sdl.K_BACKSPACE:
		gui.editor.OnKeyboard(KEY_BACKSPACE)
	case sdl.K_DELETE:
		gui.editor.OnKeyboard(KEY_DELETE)
	case sdl.K_UP:
		gui.editor.OnKeyboard(KEY_UP)
	case sdl.K_DOWN:
		gui.editor.OnKeyboard(KEY_DOWN)
	case sdl.K_LEFT:
		gui.editor.OnKeyboard(KEY_LEFT)
	case sdl.K_RIGHT:
		gui.editor.OnKeyboard(KEY_RIGHT)
	case sdl.K_ESCAPE:
		gui.editor.OnKeyboard(KEY_ESCAPE)
	}
	return true
}

func (gui *GuiManager) TextEvent(ev *sdl.TextInputEvent) bool {
	bytes := []byte{}
	for _, char := range ev.Text {
		if char != 0 {
			bytes = append(bytes, char)
		} else {
			break
		}
	}
	if len(bytes) != 0 {
		runes := []rune(string(bytes))
		if len(runes) == 1 {
			if gui.editor != nil {
				gui.editor.OnRune(runes[0])
			}
		} else {
			fmt.Println("Bad text input event")
		}
	}
	return true
}

func NewGuiManager(surface *sdl.Surface, font *ttf.Font) *GuiManager {
	return &GuiManager{
		surface: surface,
		font:    font,
	}
}
