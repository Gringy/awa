package gui

import (
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

const (
	CURSOR_SIZE = 2
)

type char struct {
	sym rune
	pre *sdl.Surface
}

func (char *char) free() {
	if char.pre != nil {
		char.pre.Free()
		char.pre = nil
	}
}

func (char *char) update(font *ttf.Font) {
	asString := string([]rune{char.sym})
	char.free()
	pre, err := font.RenderUTF8_Solid(asString, sdl.Color{A: 0xFF, R: 0xFF, G: 0xFF, B: 0xFF})
	if err != nil {
		panic(err)
	}
	char.pre = pre
}

// Struct TextInput
type TextInput struct {
	chars    []*char
	gui      *GuiManager
	position int
	font     *ttf.Font
	onSubmit func()
}

func (input *TextInput) PreferredPosition(pos sdl.Point) sdl.Point {
	return sdl.Point{}
}

func (input *TextInput) Size() sdl.Point {
	ret := sdl.Point{0, int32(input.font.Height())}
	for _, char := range input.chars {
		if char != nil && char.pre != nil {
			ret.X += char.pre.W
		}
	}
	if input.gui.editor == input {
		ret.X += CURSOR_SIZE
	}
	if ret.X < 50 {
		ret.X = 50
	}
	return ret
}

func (input *TextInput) Draw(context *Context) {
	context.FillColor(sdl.Rect{0, 0, context.area.W, context.area.H}, 0xFF555555)
	isSelected := false
	if input.gui.editor == input {
		isSelected = true
	}
	x := int32(0)
	height := int32(input.font.Height())
	for i, char := range input.chars {
		if i == input.position && isSelected {
			context.FillColor(sdl.Rect{x, 0, CURSOR_SIZE, height}, 0xFFFF0000)
			x += CURSOR_SIZE
		}
		context.DrawImage(&sdl.Rect{x, 0, char.pre.W, char.pre.H}, nil, char.pre)
		x += char.pre.W
	}
	if input.position == len(input.chars) && isSelected {
		context.FillColor(sdl.Rect{x, 0, CURSOR_SIZE, height}, 0xFFFF0000)
	}
}

func (input *TextInput) Visible() bool {
	return true
}

func (input *TextInput) OnMouseEvent(click sdl.Point, tp MouseEventType) bool {
	if tp == MOUSEUP {
		input.gui.editor = input
	}
	return true
}

func (input *TextInput) Free() {
	for _, char := range input.chars {
		char.free()
	}
	input.chars = nil
	input.position = 0
}

func (input *TextInput) SetText(text string) {
	input.Free()
	runes := []rune(text)
	for _, rn := range runes {
		char := char{sym: rn}
		char.update(input.font)
		input.chars = append(input.chars, &char)
	}
	if input.position > len(runes) {
		input.position = len(runes)
	}
}

func (input *TextInput) GetText() string {
	runes := make([]rune, len(input.chars))
	for i, char := range input.chars {
		runes[i] = char.sym
	}
	return string(runes)
}

func (input *TextInput) SetSubmitFunc(fn func()) *TextInput {
	input.onSubmit = fn
	return input
}

func (input *TextInput) OnKeyboard(k Key) {
	switch k {
	case KEY_ESCAPE:
		input.gui.editor = nil
	case KEY_LEFT:
		input.position--
		if input.position < 0 {
			input.position = 0
		}
	case KEY_RIGHT:
		input.position++
		if input.position > len(input.chars) {
			input.position = len(input.chars)
		}
	case KEY_BACKSPACE:
		input.position--
		if input.position < 0 {
			input.position = 0
			break
		}
		input.chars[input.position].free()
		input.chars = append(input.chars[:input.position], input.chars[input.position+1:]...)
	case KEY_DELETE:
		if input.position >= len(input.chars) {
			break
		}
		input.chars[input.position].free()
		input.chars = append(input.chars[:input.position], input.chars[input.position+1:]...)
	case KEY_ENTER:
		if input.onSubmit != nil {
			input.onSubmit()
		}
	}
}

func (input *TextInput) OnRune(r rune) {
	char := char{sym: r}
	char.update(input.font)
	oldChars := input.chars
	input.chars = nil
	input.chars = append(input.chars, oldChars[:input.position]...)
	input.chars = append(input.chars, &char)
	input.chars = append(input.chars, oldChars[input.position:]...)
	input.position++
}

func NewTextInput(font *ttf.Font, gui *GuiManager) *TextInput {
	return &TextInput{
		font: font,
		gui:  gui,
	}
}
