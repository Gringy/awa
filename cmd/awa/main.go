// Command awa runs server.
package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"os"

	"bitbucket.org/gringy/awa/common"
	"bitbucket.org/gringy/awa/net"
	"bitbucket.org/gringy/awa/world"
)

var toLog = flag.String("log", "", "file to write logs")

func main() {
	flag.Parse()
	if *toLog != "" {
		file, err := os.OpenFile(*toLog, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0660)
		if err != nil {
			log.Panic(err)
		}
		os.Stdout.Close()
		os.Stderr.Close()
		os.Stdout = file
		os.Stderr = file
		log.SetOutput(os.Stdout)

	}
	// Parsing config
	var config common.Config
	configBytes, err := ioutil.ReadFile("config.json")
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(configBytes, &config)
	if err != nil {
		panic(err)
	}

	// Loading services
	log.Print("Awa server " + Version)
	hub := common.Hub{
		Config: config,
	}
	common.MainHub = &hub
	net, err := net.NewNet(&hub)
	if err != nil {
		panic(err)
	}
	world, err := world.NewWorld(&hub)
	if err != nil {
		panic(err)
	}

	// Start server
	go net.Start()
	world.Start()
}
