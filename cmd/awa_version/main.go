// Command awa_version increments version in cmd/awa/version.go file
package main

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

var major = flag.Bool("major", false, "increase major version")
var minor = flag.Bool("minor", false, "increase minor version")

func findIntIn(str string) (int, error) {
	splitted := strings.Fields(str)
	for _, field := range splitted {
		if ret, err := strconv.ParseInt(field, 10, 32); err == nil {
			return int(ret), nil
		}
	}
	return 0, errors.New("String has no ints inside")
}

func main() {
	flag.Parse()

	bytes, err := ioutil.ReadFile("cmd/awa/version.go")
	if err != nil {
		panic(err)
	}
	asString := string(bytes)

	lines := strings.FieldsFunc(asString, func(r rune) bool {
		if r == '\n' || r == '\r' {
			return true
		}
		return false
	})
	prevMajor, prevMinor, prevBuild := 0, 0, 0
	for _, line := range lines {
		switch {
		case strings.Contains(line, "Major"):
			if prevMajor, err = findIntIn(line); err != nil {
				panic(err)
			}
		case strings.Contains(line, "Minor"):
			if prevMinor, err = findIntIn(line); err != nil {
				panic(err)
			}
		case strings.Contains(line, "Build"):
			if prevBuild, err = findIntIn(line); err != nil {
				panic(err)
			}
		}
	}
	if *major {
		prevMajor++
	}
	if *minor {
		prevMinor++
	}
	prevBuild++
	log.Printf("New Version: %d.%d:%d", prevMajor, prevMinor, prevBuild)
	output := "package main\n\n"
	output += "const (\n"
	output += fmt.Sprintf("\tVersion = \"v%d.%d:%d\"\n", prevMajor, prevMinor, prevBuild)
	output += fmt.Sprintf("\tMajor = %d\n", prevMajor)
	output += fmt.Sprintf("\tMinor = %d\n", prevMinor)
	output += fmt.Sprintf("\tBuild = %d\n", prevBuild)
	output += ")\n"

	ioutil.WriteFile("cmd/awa/version.go", []byte(output), 0644)
}
