package lang

import (
	"errors"
)

type Vocab struct {
	vocab []*Word
}

func (vocab *Vocab) Find(name string) (*Word, error) {
	for _, word := range vocab.vocab {
		if word.Name == name {
			return word, nil
		}
	}
	return nil, errors.New("Word not found")
}

func (vocab *Vocab) Add(word *Word) {
	for i, wrd := range vocab.vocab {
		if wrd.Name == word.Name {
			vocab.vocab[i] = word
			return
		}
	}
	vocab.vocab = append(vocab.vocab, word)
}

type Word struct {
	Name      string
	ToCompile Value
}

func boolToInt(b bool) int {
	if b {
		return 1
	} else {
		return 0
	}
}

var defaultVocab = &Vocab{
	vocab: []*Word{
		&Word{"print", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			tp := runner.Pop()
			str := tp.String()
			runner.msgs = append(runner.msgs, str)
			//fmt.Println(tp.String())
		}}},
		&Word{"apply", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			val := runner.Pop()
			apply(runner, val)
		}}},
		&Word{"+", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			right := runner.PopInt()
			left := runner.PopInt()
			runner.Push(Value{Tp: TYPE_INT, Number: left + right})
		}}},
		&Word{"-", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			right := runner.PopInt()
			left := runner.PopInt()
			runner.Push(Value{Tp: TYPE_INT, Number: left - right})
		}}},
		&Word{"*", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			right := runner.PopInt()
			left := runner.PopInt()
			runner.Push(Value{Tp: TYPE_INT, Number: left * right})
		}}},
		&Word{"/", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			right := runner.PopInt()
			left := runner.PopInt()
			runner.Push(Value{Tp: TYPE_INT, Number: left / right})
		}}},
		&Word{"%", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			right := runner.PopInt()
			left := runner.PopInt()
			runner.Push(Value{Tp: TYPE_INT, Number: left % right})
		}}},
		&Word{"if", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			quote := runner.Pop()
			if quote.Tp != TYPE_LIST {
				panic(errors.New("argument of if must be a list"))
			}
			q := runner.PopInt()
			if q != 0 {
				apply(runner, quote)
			}
		}}},
		// {test-quote body-quote -> }
		&Word{"while", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			body := runner.Pop()
			if body.Tp != TYPE_LIST {
				panic(errors.New("body-quote of while must be a list"))
			}
			test := runner.Pop()
			if test.Tp != TYPE_LIST {
				panic(errors.New("test-quote of while must be a list"))
			}
			runner.runstack = append(runner.runstack, &Runnable{Tp: RUN_WHILE, Body: body.List, Test: test.List})
		}}},
		&Word{"for", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			body := runner.Pop()
			if body.Tp != TYPE_LIST {
				panic(errors.New("body-quote of for must be a list"))
			}
			test := runner.Pop()
			if test.Tp != TYPE_LIST {
				panic(errors.New("test-quote of for must be a list"))
			}
			runner.runstack = append(runner.runstack, &Runnable{Tp: RUN_FOR, Body: body.List, Test: test.List, State: WHILE_TESTING})
			runner.Push(Value{Tp: TYPE_INT, Number: 0})
		}}},
		&Word{"i", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			top := runner.runstack[len(runner.runstack)-1]
			runner.Push(Value{Tp: TYPE_INT, Number: top.I})
		}}},
		&Word{"==", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			right := runner.PopInt()
			left := runner.PopInt()
			runner.Push(Value{Tp: TYPE_INT, Number: boolToInt(left == right)})
		}}},
		&Word{"!=", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			right := runner.PopInt()
			left := runner.PopInt()
			runner.Push(Value{Tp: TYPE_INT, Number: boolToInt(left != right)})
		}}},
		&Word{">", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			right := runner.PopInt()
			left := runner.PopInt()
			runner.Push(Value{Tp: TYPE_INT, Number: boolToInt(left > right)})
		}}},
		&Word{"<", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			right := runner.PopInt()
			left := runner.PopInt()
			runner.Push(Value{Tp: TYPE_INT, Number: boolToInt(left < right)})
		}}},
		&Word{">=", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			right := runner.PopInt()
			left := runner.PopInt()
			runner.Push(Value{Tp: TYPE_INT, Number: boolToInt(left >= right)})
		}}},
		&Word{"<=", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			right := runner.PopInt()
			left := runner.PopInt()
			runner.Push(Value{Tp: TYPE_INT, Number: boolToInt(left <= right)})
		}}},
		&Word{"len", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			a := runner.Pop()
			if a.Tp != TYPE_LIST {
				panic(errors.New("appended value must be a list"))
			}
			runner.Push(Value{Tp: TYPE_INT, Number: len(a.List)})
		}}},
		// {a-list b -> a-list-with-b}
		&Word{"append", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			b := runner.Pop()
			list := runner.Pop()
			if list.Tp != TYPE_LIST {
				panic(errors.New("appended value must be a list"))
			}
			list.List = append(list.List, b)
		}}},
		&Word{"drop", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			runner.Pop()
		}}},
		&Word{"swap", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			a := runner.Pop()
			b := runner.Pop()
			runner.Push(a)
			runner.Push(b)
		}}},
		&Word{"dup", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			a := runner.Pop()
			runner.Push(a)
			runner.Push(a)
		}}},
		&Word{"copy", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			a := runner.Pop()
			if a.Tp != TYPE_LIST {
				panic(errors.New("copied value must be a list"))
			}
			b := Value{Tp: TYPE_LIST}
			b.List = append(b.List, a.List...)
			runner.Push(a)
			runner.Push(b)
		}}},
		&Word{"nop", Value{Tp: TYPE_FUNC, Func: func(runner *Runner) {
			//
		}}},
	},
}
