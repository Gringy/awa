package lang

import (
	"testing"
)

func TestParse(t *testing.T) {
	prog, err := Parse([]byte("1 2 +"))
	if err != nil {
		t.Fatal(err)
	}
	runner := NewRunner(prog)
	if len(runner.prog) != 3 {
		t.Fatalf("size of prog != 4 but is %d", len(runner.prog))
	}
	runner.Run()
	if ret := runner.PopInt(); ret != 3 {
		t.Fatalf("runner returns %d != 3", ret)
	}
}

func TestQuote(t *testing.T) {
	prog, err := Parse([]byte("[1 2 +] apply"))
	if err != nil {
		t.Fatal(err)
	}
	runner := NewRunner(prog)
	runner.Run()
	if ret := runner.PopInt(); ret != 3 {
		t.Fatalf("runner returns %d != 3", ret)
	}
}

func TestIf(t *testing.T) {
	prog, err := Parse([]byte("1 [128 8 /]if 0[128 8 /]if"))
	if err != nil {
		t.Fatal(err)
	}
	runner := NewRunner(prog)
	runner.Run()
	if ret := runner.PopInt(); ret != 16 {
		t.Fatalf("runner returns %d != 16", ret)
	}
	if runner.StackSize() != 0 {
		t.Fatalf("stack must be empty")
	}
}

func TestWhile(t *testing.T) {
	prog, err := Parse([]byte("0[dup 9 <][dup 1 +]while"))
	if err != nil {
		t.Fatal(err)
	}
	runner := NewRunner(prog)
	runner.Run()
	if runner.StackSize() != 10 {
		t.Fatal("must be 10 values after while running")
	}
	for i := 9; i >= 0; i-- {
		tested := runner.PopInt()
		if tested != i {
			t.Fatal("bad sequence is stack")
		}
	}
	if runner.StackSize() != 0 {
		t.Fatal("must be 0 values is stack after test")
	}
}

func TestFor(t *testing.T) {
	prog, err := Parse([]byte("0[10 <][i +]for"))
	if err != nil {
		t.Fatal(err)
	}
	runner := NewRunner(prog)
	runner.Run()
	if runner.StackSize() != 1 {
		t.Fatal("there is too much elements in stack after for running")
	}
	if ret := runner.PopInt(); ret != 45 {
		t.Fatalf("runner returns %d != 45", ret)
	}
}
