package lang

import (
	"errors"
	"strconv"
)

func isBlank(rn rune) bool {
	return rn == ' ' || rn == '\n' || rn == '\t' || rn == '\r'
}

func GetValue(name string) (Value, error) {
	if word, err := defaultVocab.Find(name); err == nil {
		return word.ToCompile, nil
	}
	if ret, err := strconv.ParseInt(name, 10, 64); err == nil {
		return Value{Tp: TYPE_INT, Number: int(ret)}, nil
	}
	return Value{}, errors.New("cannot find word")
}

func Parse(bytes []byte) ([]Value, error) {
	runes := []rune(string(bytes) + " ")
	prog := []Value{}
	contexts := []Value{}
	name := ""
	addContext := func() {
		contexts = append(contexts, Value{Tp: TYPE_LIST})
	}
	compile := func(val Value) {
		if len(contexts) == 0 {
			prog = append(prog, val)
			return
		}
		ln := len(contexts)
		contexts[ln-1].List = append(contexts[ln-1].List, val)
	}
	compileContext := func() {
		conLen := len(contexts)
		if conLen >= 2 {
			last := contexts[conLen-1]
			contexts = contexts[:conLen-1]
			conLen = len(contexts)
			contexts[conLen-1].List = append(contexts[conLen-1].List, last)
		} else {
			last := contexts[0]
			contexts = nil
			prog = append(prog, last)
		}
	}
	compileWord := func() error {
		val, err := GetValue(name)
		if err != nil {
			return err
		}
		name = ""
		compile(val)
		return nil
	}
	for _, rn := range runes {
		switch {
		case name == "" && isBlank(rn):
			continue
		case name != "" && isBlank(rn):
			err := compileWord()
			if err != nil {
				return nil, err
			}
		case name == "" && rn == '[':
			addContext()
		case name == "" && rn == ']':
			if len(contexts) != 0 {
				compileContext()
			} else {
				return nil, errors.New("end brace without begin brace")
			}
		case name != "" && rn == '[':
			err := compileWord()
			if err != nil {
				return nil, err
			}
			addContext()
		case name != "" && rn == ']':
			err := compileWord()
			if err != nil {
				return nil, err
			}
			if len(contexts) != 0 {
				compileContext()
			} else {
				return nil, errors.New("end brace without begin brace")
			}
		default:
			name += string([]rune{rn})
		}
	}
	return prog, nil
}
