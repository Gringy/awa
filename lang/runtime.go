package lang

import "fmt"

type Runner struct {
	stack    []Value
	runstack []*Runnable
	prog     []Value
	msgs     []string
}

func (runner *Runner) Run() {
	runner.runstack = append(runner.runstack, &Runnable{Tp: RUN_LIST, Body: runner.prog})
	for len(runner.runstack) != 0 {
		runner.runstack[len(runner.runstack)-1].Do(runner)
	}
}

func (runner *Runner) RunProg(prog []Value) {
	runner.prog = prog
	runner.Run()
}

func (runner *Runner) StackSize() int {
	return len(runner.stack)
}

func (runner *Runner) DropMessages() []string {
	ret := runner.msgs
	runner.msgs = nil
	return ret
}

func (runner *Runner) Pop() Value {
	if len(runner.stack) == 0 {
		panic("Stack underflow")
	}
	ret := runner.stack[len(runner.stack)-1]
	runner.stack = runner.stack[:len(runner.stack)-1]
	return ret
}

func (runner *Runner) PopInt() int {
	if len(runner.stack) == 0 {
		panic("Stack underflow")
	}
	ret := runner.stack[len(runner.stack)-1]
	runner.stack = runner.stack[:len(runner.stack)-1]
	if ret.Tp != TYPE_INT {
		panic("error: elem must be int")
	}
	return ret.Number
}

func (runner *Runner) Pick() *Value {
	if len(runner.stack) == 0 {
		panic("Stack underflow when pick")
	}
	return &runner.stack[len(runner.stack)-1]
}

func (runner *Runner) Push(tp Value) {
	runner.stack = append(runner.stack, tp)
}

func (runner *Runner) DropRun() {
	if len(runner.runstack) == 0 {
		panic("Runstack underflow")
	}
	runner.runstack = runner.runstack[:len(runner.runstack)-1]
}

func NewEmptyRunner() *Runner {
	return &Runner{}
}

func NewRunner(prog []Value) *Runner {
	return &Runner{prog: prog}
}

const (
	TYPE_OTHER uint16 = iota
	TYPE_FUNC
	TYPE_LIST
	TYPE_INT
	TYPE_STRING
)

type Value struct {
	Tp     uint16
	Str    string
	Number int
	List   []Value
	Func   func(*Runner)
	Other  interface{}
}

func (tp *Value) Apply(runner *Runner) {
	switch tp.Tp {
	case TYPE_FUNC:
		tp.Func(runner)
	default:
		runner.Push(*tp)
	}
}

func (val *Value) String() string {
	switch val.Tp {
	case TYPE_INT:
		return fmt.Sprintf("%d", val.Number)
	case TYPE_LIST:
		ret := "["
		for i, some := range val.List {
			ret += some.String()
			if i != len(val.List)-1 {
				ret += " "
			}
		}
		ret += "]"
		return ret
	case TYPE_FUNC:
		return "<func>"
	case TYPE_STRING:
		return fmt.Sprintf("\"%s\"", val.Str)
	}
	return "<some>"
}

const (
	RUN_LIST uint16 = iota
	RUN_WHILE
	RUN_FOR
)

type Runnable struct {
	Tp    uint16
	State uint16
	Body  []Value
	Test  []Value
	I     int
	Index int
}

const (
	WHILE_TESTING uint16 = iota
	WHILE_BODY
)

func (r *Runnable) Do(runner *Runner) {
	switch r.Tp {
	case RUN_LIST:
		if r.Index >= len(r.Body) {
			runner.DropRun()
			return
		}
		r.Body[r.Index].Apply(runner)
		r.Index++
	case RUN_WHILE, RUN_FOR:
		if r.State == WHILE_BODY && r.Index >= len(r.Body) {
			r.Index = 0
			r.State = WHILE_TESTING
			if r.Tp == RUN_FOR {
				r.I++
				runner.Push(Value{Tp: TYPE_INT, Number: r.I})
			}
			return
		}
		if r.State == WHILE_BODY {
			r.Body[r.Index].Apply(runner)
			r.Index++
			return
		}
		if r.State == WHILE_TESTING && r.Index >= len(r.Test) {
			cond := runner.PopInt()
			if cond != 0 {
				r.Index = 0
				r.State = WHILE_BODY
			} else {
				runner.DropRun()
			}
			return
		}
		if r.State == WHILE_TESTING {
			r.Test[r.Index].Apply(runner)
			r.Index++
			return
		}
	default:
		panic("unknown type of runnable")
	}
}

func apply(runner *Runner, val Value) {
	switch val.Tp {
	case TYPE_LIST:
		runner.runstack = append(runner.runstack, &Runnable{Tp: RUN_LIST, Body: val.List})
	default:
		panic("error: only list can be applied")
	}
}
