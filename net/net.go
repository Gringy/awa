// Package net contains implementation of message receiving and sending via
// websocket protocol and hides it.
package net

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"

	"bitbucket.org/gringy/awa/common"
)

// Implementation of common.Net
type Net struct {
	hub *common.Hub
}

// Starts listening and serving http connection. Blocking.
func (net *Net) Start() {
	log.Print("Listen at " + net.hub.Config.Port)
	err := http.ListenAndServe(net.hub.Config.Port, nil)
	if err != nil {
		panic(err)
	}
}

// Makes new Net instanse.
func NewNet(hub *common.Hub) (common.Net, error) {
	log.Print("Start net initialization")

	net := new(Net)

	net.hub = hub
	hub.Net = net

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Print("NewNet: Handler: connection")
		ws, err := websocket.Upgrade(w, r, nil, 1024, 1024)
		if err != nil {
			w.Write([]byte(err.Error()))
			log.Print(err.Error())
			return
		}

		client := NewClient(ws)
		id, err := client.Login(hub)
		if err != nil {
			log.Print(err)
			return
		}
		client.playerID = id
		hub.World.Registered(client)
		go client.ReadPump(hub)
		go client.WritePump(hub)
		client.BufPump(hub)
	})

	log.Print("End net initialization")
	return net, nil
}
