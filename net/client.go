package net

import (
	"log"

	"github.com/gorilla/websocket"

	"bitbucket.org/gringy/awa/common"
)

// Client struct is an implementation of common.Client
type Client struct {
	ws       *websocket.Conn
	toSend   chan common.Message
	toWrite  chan common.Message
	err      chan bool
	playerID common.PlayerID
}

// NewClient function creates new client from websocket connection
func NewClient(ws *websocket.Conn) *Client {
	return &Client{
		ws:      ws,
		toSend:  make(chan common.Message),
		toWrite: make(chan common.Message),
		err:     make(chan bool),
	}
}

func (cl *Client) Error() {
	cl.err <- true
}

// Send function. Is not blocking.
func (cl *Client) Send(msg common.Message) {
	cl.toSend <- msg
}

// Login function
func (cl *Client) Login(hub *common.Hub) (id common.PlayerID, err error) {
	_, bytes, err := cl.ws.ReadMessage()
	if err != nil {
		return 0, err
	}
	return ParseLogin(bytes)
}

func (cl *Client) PlayerID() common.PlayerID {
	return cl.playerID
}

// WritePump function reads messages from input channel and writes it in socket
func (cl *Client) WritePump(hub *common.Hub) {
	defer func() {
		hub.World.Unregistered(cl)
		close(cl.toWrite)
		close(cl.toSend)
		close(cl.err)
		cl.ws.Close()
	}()
	for {
		select {
		case msg, ok := <-cl.toWrite:
			if !ok {
				log.Print("(*Client).WritePump: toWrite closed")
				return
			}
			err := cl.ws.WriteMessage(websocket.BinaryMessage, msg.Bytes)
			if err != nil {
				log.Print("(*Client).WritePump: error on websocket:", err)
				return
			}
		case <-cl.err:
			log.Print("(*Client).WritePump: error received")
			return
		}
	}
}

// ReadPump function
func (cl *Client) ReadPump(hub *common.Hub) {
	for {
		mt, bytes, err := cl.ws.ReadMessage()
		if mt != websocket.BinaryMessage || err != nil {
			log.Print("(*Client).ReadPump: Error on websocket")
			break
		}
		msg, err := ParseCommand(cl.playerID, bytes)
		if err != nil {
			log.Print("(*Client).ReadPump: cannot parse message: ", err)
			break
		}
		hub.World.Command(msg)
	}
}

// BufPump function
func (cl *Client) BufPump(hub *common.Hub) {
	buf := make([]common.Message, 10)
	bufStart := 0
	bufLen := 0
	for {
		if bufLen == 0 {
			select {
			case msg, ok := <-cl.toSend:
				if !ok {
					log.Print("(*Client).BufPump: toSend closed")
					return
				}
				index := (bufStart + bufLen) % 10
				buf[index] = msg
				bufLen++
			}
		} else {
			select {
			case msg, ok := <-cl.toSend:
				if !ok {
					log.Print("(*Client).BufPump: toSend closed")
					return
				}
				if bufLen >= 10 {
					log.Print("(*Client).BufPump: buffer overflow")
					cl.Error()
					return
				}
				index := (bufStart + bufLen) % 10
				buf[index] = msg
				bufLen++
			case cl.toWrite <- buf[bufStart]:
				bufStart = (bufStart + 1) % 10
				bufLen--
			}
		}
	}
}
