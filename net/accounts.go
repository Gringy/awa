package net

import (
	"encoding/json"
	"io/ioutil"
	"log"

	"bitbucket.org/gringy/awa/common"
)

type account struct {
	Login string          `json:"login"`
	Pass  string          `json:"pass"`
	Pid   common.PlayerID `json:"pid"`
}

func validateAccount(login, pass string) (common.PlayerID, bool) {
	fileBytes, err := ioutil.ReadFile("accounts.json")
	if err != nil {
		log.Print("validateAccount: error: ", err)
		return 0, false
	}
	var accounts []account
	err = json.Unmarshal(fileBytes, &accounts)
	if err != nil {
		log.Print("validateAccount: error: ", err)
		return 0, false
	}
	for _, account := range accounts {
		if account.Login == login && account.Pass == pass {
			log.Printf("validateAccount: %s %s", login, pass)
			return account.Pid, true
		}
	}
	return 0, false
}
