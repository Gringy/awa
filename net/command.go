package net

import (
	"errors"

	"bitbucket.org/gringy/awa/common"
	"bitbucket.org/gringy/awa/common/binary"
	"bitbucket.org/gringy/awa/common/msg"
)

// ParseCommand function
func ParseCommand(id common.PlayerID, bytes []byte) (cmd common.Command, err error) {
	// for must's panics
	defer func() {
		if some := recover(); some != nil {
			casted := some.(error)
			cmd = nil
			err = casted
		}
	}()
	if len(bytes) == 0 {
		return nil, errors.New("ParseCommand: Zero length message")
	}
	reader := binary.NewReader(bytes)
	tp, err := reader.ReadByte()
	if err != nil {
		return nil, err
	}
	switch tp {
	case 0x00:
		// Say
		str, err := reader.ReadString()
		if err != nil {
			return nil, err
		}
		return &Say{Command{id}, str}, nil
	case 0x01:
		// Move
		dx, err := reader.ReadByte()
		if err != nil {
			return nil, err
		}
		dy, err := reader.ReadByte()
		if err != nil {
			return nil, err
		}
		return &Move{Command{id}, msg.Byte2signed(dx), msg.Byte2signed(dy)}, nil
	case 0x02:
		// Replace
		dx, err := reader.ReadByte()
		if err != nil {
			return nil, err
		}
		dy, err := reader.ReadByte()
		if err != nil {
			return nil, err
		}
		tp, err := reader.ReadShort()
		if err != nil {
			return nil, err
		}
		return &Replace{Command{id}, msg.Byte2signed(dx), msg.Byte2signed(dy), tp}, nil
	case 0x03:
		// descriptions request
		count, err := reader.ReadShort()
		if err != nil {
			return nil, err
		}
		ids := make([]uint16, count)
		for i := 0; i < int(count); i++ {
			tp, err := reader.ReadShort()
			if err != nil {
				return nil, err
			}
			ids[i] = tp
		}
		return &BlockRequest{Command{id}, ids}, nil
	case 0x04:
		// action request
		actionType, err := reader.ReadByte()
		if err != nil {
			return nil, err
		}
		chunkID, err := reader.ReadInt()
		if err != nil {
			return nil, err
		}
		objID, err := reader.ReadInt()
		if err != nil {
			return nil, err
		}
		return &ActionRequest{Command{id}, common.Action(actionType),
			common.ChunkID(chunkID), common.ID(objID)}, nil
	case 0x05:
		// move item
		fromID := reader.MustInt()
		fromX := reader.MustByte()
		fromY := reader.MustByte()
		toID := reader.MustInt()
		toX := reader.MustByte()
		toY := reader.MustByte()
		return &ItemMove{Command: Command{id},
			fromID:    common.ID(fromID),
			fromPoint: common.Point{int(fromX), int(fromY)},
			toID:      common.ID(toID),
			toPoint:   common.Point{int(toX), int(toY)},
		}, nil
	case 0x06:
		// action on item
		return nil, errors.New("ParseCommand: action on item not implemented")
	case 0x07:
		// item description request
		count := reader.MustShort()
		itemIDs := make([]uint32, count)
		for i := range itemIDs {
			itemIDs[i] = reader.MustInt()
		}
		return &ItemDescRequest{Command{id}, itemIDs}, nil
	default:
		return nil, errors.New("ParseCommand: Unknown message")
	}
}

// ParseLogin function
func ParseLogin(bytes []byte) (common.PlayerID, error) {
	reader := binary.NewReader(bytes)
	login, err := reader.ReadString()
	if err != nil {
		return 0, err
	}
	pass, err := reader.ReadString()
	if err != nil {
		return 0, err
	}
	if id, ok := validateAccount(login, pass); ok {
		return id, nil
	} else {
		return 0, errors.New("ParseLogin: bad login or password")
	}
}

// Command struct
type Command struct {
	playerID common.PlayerID
}

// ID function
func (c *Command) PlayerID() common.PlayerID {
	return c.playerID
}

// Say struct
type Say struct {
	Command
	say string
}

// Say.Do function
func (m *Say) Do(p common.Player, c common.Chunk) {
	p.SayMsg(c, m.say)
}

type Move struct {
	Command
	dx, dy int
}

func (m *Move) Do(p common.Player, c common.Chunk) {
	p.MoveMsg(c, m.dx, m.dy)
}

type Replace struct {
	Command
	dx, dy int
	tp     uint16
}

func (r *Replace) Do(p common.Player, c common.Chunk) {
	p.ReplaceMsg(c, r.dx, r.dy, r.tp)
}

type BlockRequest struct {
	Command
	ids []uint16
}

func (b *BlockRequest) Do(p common.Player, c common.Chunk) {
	p.BlockRequestMsg(c, b.ids)
}

type ActionRequest struct {
	Command
	actionType common.Action
	chunkID    common.ChunkID
	objectID   common.ID
}

func (ar *ActionRequest) Do(p common.Player, c common.Chunk) {
	p.ActionRequestMsg(c, ar.actionType, ar.chunkID, ar.objectID)
}

type ItemMove struct {
	Command
	fromID    common.ID
	fromPoint common.Point
	toID      common.ID
	toPoint   common.Point
}

func (im *ItemMove) Do(p common.Player, c common.Chunk) {
	p.ItemMoveMsg(c, im.fromID, im.fromPoint, im.toID, im.toPoint)
}

type ItemDescRequest struct {
	Command
	ids []uint32
}

func (idr *ItemDescRequest) Do(p common.Player, c common.Chunk) {
	p.ItemDescRequestMsg(c, idr.ids)
}
