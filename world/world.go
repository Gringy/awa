// Package world contains SOMETHING AWFUL.
package world

import (
	"errors"
	"log"
	"math/rand"
	"time"

	"bitbucket.org/gringy/awa/common"
	"bitbucket.org/gringy/awa/common/msg"
)

// World struct is an Implementation of common.World interface
type World struct {
	// Hub to communicate with net and other
	hub *common.Hub

	// Chan to receive messages from clients
	cmd chan common.Command
	// Chan to accept new connections
	registered chan common.Client
	// Chan to accept closed connections
	unregistered chan common.Client

	// Slice of clients
	clients []WorldClient

	// Slice of chunks
	chunks []common.Chunk

	// Current free gameID
	currentID common.ID

	// Types Manager
	mgr common.TypeManager
}

// WorldClient struct
type WorldClient struct {
	client   common.Client
	playerID common.PlayerID
	chunkID  common.ChunkID
	gameID   common.ID
}

// Start function starts ticking the world. Blocking.
func (world *World) Start() {
	ticker := time.NewTicker(time.Millisecond *
		time.Duration(world.hub.Config.TurnMillis))
	for {
		select {
		case <-ticker.C:
			for _, chunk := range world.chunks {
				chunk.Tick()
			}
			for _, chunk := range world.chunks {
				chunk.SendDelta()
			}
			for _, chunk := range world.chunks {
				chunk.ClearDelta()
			}
		case client := <-world.registered:
			log.Print("(*World).Start: registered: playerID:", client.PlayerID())
			world.addClient(client)
			world.sendInit(client)
		case client := <-world.unregistered:
			log.Print("(*World).Start: unregistered: playerID:", client.PlayerID())
			world.delClient(client)
		case cmd := <-world.cmd:
			world.doCommand(cmd)
		}
	}
}

func (world *World) NextGameID() common.ID {
	ret := world.currentID
	world.currentID++
	return ret
}

// PlayerMovedTo function used when client change chunk to
// be able to send messages from world instance
func (world *World) PlayerMovedTo(player common.PlayerID, chunk common.ChunkID) {
	for i := range world.clients {
		if world.clients[i].playerID == player {
			world.clients[i].chunkID = chunk
			return
		}
	}
}

// doCommand function is called when world receive message,
// it finds player struct and call player.Do with this command
func (world *World) doCommand(cmd common.Command) {
	playerID := cmd.PlayerID()
	worldClient := world.getWorldClient(playerID)
	chunk := world.chunks[worldClient.chunkID]
	player, err := chunk.GetPlayer(playerID)
	if err != nil {
		log.Print("(*World).doCommand: ", err)
		return
	}
	cmd.Do(player, chunk)
}

// sendInit function is called when player is just connected
func (world *World) sendInit(client common.Client) {
	playerID := client.PlayerID()
	worldClient := world.getWorldClient(playerID)
	chunk, err := world.Chunk(worldClient.chunkID)
	if err != nil {
		log.Println("(*World).sendInit: ", err)
	}
	player, err := chunk.GetPlayer(playerID)
	if err != nil {
		log.Print("(*World).sendInit: ", err)
		return
	}
	//
	player.Client().Send(common.Message{
		Bytes: msg.InitMessage(player, chunk),
	})
}

// Registered function
func (world *World) Registered(client common.Client) {
	world.registered <- client
}

// Unregistered function
func (world *World) Unregistered(client common.Client) {
	world.unregistered <- client
}

// Command function
func (world *World) Command(cmd common.Command) {
	world.cmd <- cmd
}

func (world *World) TypeManager() common.TypeManager {
	return world.mgr
}

func (world *World) Chunks() []common.Chunk {
	return world.chunks
}

func (world *World) Chunk(chunkID common.ChunkID) (common.Chunk, error) {
	for _, chunk := range world.chunks {
		if chunk.ChunkID() == chunkID {
			return chunk, nil
		}
	}
	return nil, errors.New("(*World).Chunk: chunk not found")
}

// addClient function adds client to clients slice
func (world *World) addClient(client common.Client) {
	for _, chunk := range world.chunks {
		if player, err := chunk.GetPlayer(client.PlayerID()); err == nil {
			worldClient := WorldClient{
				client:   client,
				playerID: client.PlayerID(),
				gameID:   player.ID(),
				chunkID:  chunk.ChunkID(),
			}
			player.SetClient(client)
			world.clients = append(world.clients, worldClient)
			break
		}
	}
}

// getWorldClient function finds struct by player id
func (world *World) getWorldClient(playerID common.PlayerID) WorldClient {
	for i := range world.clients {
		if world.clients[i].playerID == playerID {
			return world.clients[i]
		}
	}
	return WorldClient{}
}

// delClient function deletes client from clients slice
func (world *World) delClient(client common.Client) {
	index := -1
	for i, wClient := range world.clients {
		if client.PlayerID() == wClient.playerID {
			index = i
			// set player's client to nil
			chunk := world.chunks[wClient.chunkID]
			player, err := chunk.GetPlayer(wClient.playerID)
			if err != nil {
				log.Print("(*World).delClient: ", err)
				break
			}
			player.SetClient(nil)
			break
		}
	}
	if index == -1 {
		log.Print("(*World).delClient: unknown client")
		return
	}
	world.clients = append(world.clients[:index], world.clients[index+1:]...)
}

// createChunks function initialize chunks of world
func (world *World) createChunks(conf worldGenConfig) {
	// create chunks
	for i := 0; i < conf.nxn*conf.nxn; i++ {
		ch := NewChunk(common.ChunkID(i))
		world.chunks = append(world.chunks, ch)
	}
	// link chunks
	// for every chunk
	for x := 0; x < conf.nxn; x++ {
		for y := 0; y < conf.nxn; y++ {
			wrk := world.chunks[y*conf.nxn+x]
			// for every chunk around
			for dx := -1; dx <= 1; dx++ {
				for dy := -1; dy <= 1; dy++ {
					if dx != 0 || dy != 0 {
						curx := common.Normalize(x+dx, conf.nxn)
						cury := common.Normalize(y+dy, conf.nxn)
						wrk.SetNeighbour(dx, dy, world.chunks[cury*conf.nxn+curx])
					}
				}
			}
		}
	}
	generator := NewGenerator(world)
	generator.Do(world.chunks[0])
}

// NewWorld function creates and initializes new world
func NewWorld(hub *common.Hub) (common.World, error) {
	log.Print("Start world initialization")
	world := new(World)

	rand.Seed(0xDEADBEEF)

	world.cmd = make(chan common.Command)
	world.registered = make(chan common.Client)
	world.unregistered = make(chan common.Client)

	world.currentID = 1

	world.hub = hub
	hub.World = world

	mgr, err := LoadTypes()
	if err != nil {
		return nil, err
	}
	world.mgr = mgr

	worldConfig := worldGenConfig{
		nxn:       hub.Config.NXN,
		playerNum: hub.Config.Players,
	}
	world.createChunks(worldConfig)

	log.Print("End world initialization")
	return world, nil
}
