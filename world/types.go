package world

import (
	"encoding/json"
	"io/ioutil"

	"bitbucket.org/gringy/awa/common"
)

type TypeManager struct {
	byNames map[string]*common.TypeDesc
	byId    map[common.BlockType]*common.TypeDesc

	itemByName map[string]*common.ItemDesc
	itemById   map[common.ItemType]*common.ItemDesc
}

func (t *TypeManager) TypeByName(name string) *common.TypeDesc {
	return t.byNames[name]
}

func (t *TypeManager) TypeById(id common.BlockType) *common.TypeDesc {
	return t.byId[id]
}

func (t *TypeManager) TypesMap() map[common.BlockType]*common.TypeDesc {
	return t.byId
}

func (t *TypeManager) ItemByName(name string) *common.ItemDesc {
	return t.itemByName[name]
}

func (t *TypeManager) ItemById(id common.ItemType) *common.ItemDesc {
	return t.itemById[id]
}

func (t *TypeManager) ItemsMap() map[common.ItemType]*common.ItemDesc {
	return t.itemById
}

type loadStruct struct {
	Names []string `json:"names"`
	Pass  bool     `json:"pass"`
	Index byte     `json:"draw_index"`
}

type itemLoadStruct struct {
	Name      string `json:"name"`
	Stack     uint8  `json:"stack"`
	Group     string `json:"group"`
	DrawIndex uint16 `json:"draw_index"`
}

func LoadTypes() (common.TypeManager, error) {
	mgr := &TypeManager{}

	// load block types
	mgr.byNames = make(map[string]*common.TypeDesc)
	mgr.byId = make(map[common.BlockType]*common.TypeDesc)
	bytes, err := ioutil.ReadFile("blocks.json")
	if err != nil {
		return nil, err
	}
	var v []loadStruct
	err = json.Unmarshal(bytes, &v)
	if err != nil {
		return nil, err
	}
	for id, str := range v {
		desc := &common.TypeDesc{
			Id:        common.BlockType(id),
			Names:     str.Names,
			Walkable:  str.Pass,
			DrawIndex: str.Index,
		}
		mgr.byId[common.BlockType(id)] = desc
		for _, name := range str.Names {
			mgr.byNames[name] = desc
		}
	}

	// load item types
	mgr.itemByName = make(map[string]*common.ItemDesc)
	mgr.itemById = make(map[common.ItemType]*common.ItemDesc)
	bytes, err = ioutil.ReadFile("items.json")
	if err != nil {
		return nil, err
	}
	var itemV []itemLoadStruct
	err = json.Unmarshal(bytes, &itemV)
	if err != nil {
		return nil, err
	}

	for id, str := range itemV {
		desc := &common.ItemDesc{
			Name:      str.Name,
			Stack:     str.Stack,
			Type:      common.ItemType(id + 1),
			Group:     common.StringToItemGroup(str.Group),
			DrawIndex: str.DrawIndex,
		}
		mgr.itemById[desc.Type] = desc
		mgr.itemByName[desc.Name] = desc
	}

	return mgr, nil
}
