package world

import (
	"math/rand"

	"bitbucket.org/gringy/awa/common"
	"bitbucket.org/gringy/awa/world/obj"
)

const (
	MAX_LEN_OF_HALL = 25
)

type mpType int

const (
	SNOW mpType = iota
	DUNG
	WOOD
	SAND
	__LAST
)

func randStyle() mpType {
	return mpType(rand.Intn(int(__LAST)))
}

type generator struct {
	world         common.World
	rooms         []*Room
	typeManager   common.TypeManager
	currentPlayer int
}

func FixChunkSpace(chunk common.Chunk) {
	chunkMap := chunk.Map()
	grass := common.MainHub.World.TypeManager().TypeByName("grass").Id
	for x := 0; x < common.MAPDIM; x++ {
		for y := 0; y < common.MAPDIM; y++ {
			if chunkMap.GetAt(x, y).IsEmpty() {
				chunkMap.SetTypeAt(x, y, grass)
				if rand.Float64() < 0.2 {
					tree := obj.NewTree(common.MainHub.World.NextGameID())
					chunk.AddObject(tree)
					tree.Move(x, y)
				}
			}
		}
	}
}

func (gen *generator) AddRoom(room *Room) {
	gen.rooms = append(gen.rooms, room)
}

func (gen *generator) getStyle(style mpType) (wall, floor common.BlockType) {
	mgr := gen.typeManager
	switch style {
	case WOOD:
		return mgr.TypeByName("wooden wall").Id, mgr.TypeByName("wooden floor").Id
	case DUNG:
		return mgr.TypeByName("stone wall").Id, mgr.TypeByName("stone floor").Id
	case SNOW:
		return mgr.TypeByName("ice").Id, mgr.TypeByName("snow").Id
	case SAND:
		return mgr.TypeByName("sand wall").Id, mgr.TypeByName("sand").Id
	}
	return 0, 0
}

func (gen *generator) Do(starter common.Chunk) {
	totalChunks := len(gen.world.Chunks())
	gen.AddRoom(&Room{
		current: starter,
		place:   common.Point{16, 16},
		size:    3,
	})
	gen.rooms[0].MakeRoom(gen)
	for i := 0; i < 30*totalChunks; i++ {
		index := rand.Intn(len(gen.rooms))
		room := gen.rooms[index]
		room.MakeRoomAround(gen)
	}
	for i := 0; i < 3; i++ {
		for _, room := range gen.rooms {
			if !room.hasHall {
				room.MakeHalls(gen)
			}
		}
	}
	for _, room := range gen.rooms {
		room.AddObject(gen)
	}
	for _, chunk := range gen.world.Chunks() {
		FixChunkSpace(chunk)
	}
}

type Room struct {
	current common.Chunk
	place   common.Point
	size    int
	hasHall bool
}

func (room *Room) AddObject(gen *generator) {
	typeOfObject := rand.Intn(4)
	switch typeOfObject {
	case 0:
		player := NewPlayer(common.MainHub.World.NextGameID(), common.PlayerID(gen.currentPlayer))
		gen.currentPlayer++
		room.current.AddPlayer(player)
		player.Move(room.place.X, room.place.Y)
	case 1:
		mob := obj.NewMob(common.MainHub.World.NextGameID())
		room.current.AddObject(mob)
		mob.Move(room.place.X, room.place.Y)
	case 2:
		foll := obj.NewFollower(common.MainHub.World.NextGameID())
		room.current.AddObject(foll)
		foll.Move(room.place.X, room.place.Y)
	case 3:
		chest := obj.NewChest(common.MainHub.World.NextGameID(), common.Point{4, 4})
		room.current.AddObject(chest)
		chest.AddSome()
		chest.Move(room.place.X, room.place.Y)
	}
}

func (room *Room) MakeRoom(gen *generator) {
	walker := walker{
		current: room.current,
		place:   room.place,
		mgr:     gen.typeManager,
	}
	size := room.size
	wall, floor := gen.getStyle(randStyle())
	for i := -room.size; i <= room.size; i++ {
		walker.setTypeAt(i, size, wall)
		walker.setTypeAt(i, -size, wall)
		walker.setTypeAt(size, i, wall)
		walker.setTypeAt(-size, i, wall)
	}
	for x := -size + 1; x <= size-1; x++ {
		for y := -size + 1; y <= size-1; y++ {
			walker.setTypeAt(x, y, floor)
		}
	}
}

func (room *Room) MakeRoomAround(gen *generator) {
	size := common.RandInt(3, 5)
	dx := common.RandInt(5, 18)
	dy := common.RandInt(5, 18)
	if rand.Intn(2) != 0 {
		dx = -dx
	}
	if rand.Intn(2) != 0 {
		dy = -dy
	}
	walker := walker{
		current: room.current,
		place:   room.place,
		mgr:     gen.typeManager,
	}
	walker.moveTo(dx, dy)
	if walker.isSquareEmpty(size + 2) {
		newRoom := Room{
			current: walker.current,
			size:    size,
			place:   walker.place,
		}
		newRoom.MakeRoom(gen)
		gen.AddRoom(&newRoom)
	}
}

func (room *Room) MakeHalls(gen *generator) {
	totalHalls := rand.Intn(80) / 16
	for i := 0; i < totalHalls; i++ {
		room.MakeHall(gen)
	}
}

func (room *Room) MakeHall(gen *generator) {
	dir := common.RandDir()

	walker := walker{
		current: room.current,
		place:   room.place,
		mgr:     gen.typeManager,
	}
	deltaDir := common.RandDir()
	deltaN := rand.Intn(room.size)
	for i := 0; i < deltaN; i++ {
		walker.moveDir(deltaDir)
	}
	// move to wall
	for !walker.isHereWall(dir) {
		walker.moveDir(dir)
	}
	walker.moveDir(dir)
	// search if walker can go to another room
	searchWalker := walker.fork()
	i := 0
	for !searchWalker.isHereWall(dir) && i < MAX_LEN_OF_HALL {
		if searchWalker.isHere(dir) {
			return
		}
		searchWalker.moveDir(dir)
		i++
	}
	if i == MAX_LEN_OF_HALL {
		return
	}
	// make hall
	wall, floor := gen.getStyle(randStyle())
	walker.moveBack(dir)
	walker.setTypeAt(0, 0, floor)
	walker.moveDir(dir)
	for n := 0; n < i; n++ {
		walker.setTypeInLine(dir, wall, 1)
		walker.setTypeInLine(dir, floor, 0)
		walker.setTypeInLine(dir, wall, -1)
		walker.moveDir(dir)
	}
	walker.setTypeAt(0, 0, floor)
	room.hasHall = true
}

func NewGenerator(world common.World) *generator {
	return &generator{
		world:         world,
		typeManager:   world.TypeManager(),
		currentPlayer: 1,
	}
}

type worldGenConfig struct {
	nxn       int
	playerNum int
}
