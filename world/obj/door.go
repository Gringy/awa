package obj

import (
	"log"

	"bitbucket.org/gringy/awa/common"
)

type Door struct {
	gameID  common.ID
	x, y    int
	current common.Chunk
	opened  bool
}

func (door *Door) Tick(chunk common.Chunk) {
	//
}

func (door *Door) OnChangeChunk(old, new common.Chunk) {
	old.RemoveObject(door)
	old.Del(door)
	new.AddObject(door)
	new.Add(door)
	door.current = new
}

func (door *Door) ID() common.ID {
	return door.gameID
}

func (door *Door) Type() common.ObjectType {
	if door.opened {
		return common.OT_DOOR_OPENED
	} else {
		return common.OT_DOOR_CLOSED
	}
}

var doorDrawIndexes = []common.ObjectDrawIndex{
	common.ObjectDrawIndex{common.OT_DOOR_OPENED, 19},
	common.ObjectDrawIndex{common.OT_DOOR_CLOSED, 20},
}

func (door *Door) DrawIndexes() []common.ObjectDrawIndex {
	return doorDrawIndexes
}

func (door *Door) Speed() int {
	return 2
}

func (door *Door) Coord() (x, y int) {
	return door.x, door.y
}

func (door *Door) Params() common.Params {
	return common.Params{}
}

func (door *Door) Move(x, y int) {
	if x < 0 || y < 0 || x >= common.MAPDIM || y >= common.MAPDIM {
		log.Printf("(*Door).Move: bad coords: (%d,%d)", x, y)
		return
	}
	door.x = x
	door.y = y
}

func (door *Door) Action(desc common.ActionDesc) {
	//
}

func (door *Door) OnItemsShow(common.ItemsHolder) {}

func NewDoor(gameID common.ID) common.Object {
	return &Door{
		gameID: gameID,
	}
}
