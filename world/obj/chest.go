package obj

import (
	"log"

	"bitbucket.org/gringy/awa/common"
)

type Chest struct {
	gameID    common.ID
	x, y      int
	current   common.Chunk
	size      common.Point
	items     []common.Item
	listeners []common.HolderListener
}

func (chest *Chest) Tick(chunk common.Chunk) {
	//
}

func (chest *Chest) OnChangeChunk(old, new common.Chunk) {
	old.RemoveObject(chest)
	old.Del(chest)
	new.AddObject(chest)
	new.Add(chest)
	chest.current = new
}

func (chest *Chest) ID() common.ID {
	return chest.gameID
}

func (chest *Chest) Type() common.ObjectType {
	return common.OT_CHEST
}

var chestDrawIndexes = []common.ObjectDrawIndex{
	common.ObjectDrawIndex{common.OT_CHEST, 8},
}

func (chest *Chest) DrawIndexes() []common.ObjectDrawIndex {
	return chestDrawIndexes
}

func (chest *Chest) Speed() int {
	return 0
}

func (chest *Chest) Coord() (x, y int) {
	return chest.x, chest.y
}

func (chest *Chest) Params() common.Params {
	return common.Params{}
}

func (chest *Chest) Move(x, y int) {
	if x < 0 || y < 0 || x >= common.MAPDIM || y >= common.MAPDIM {
		log.Printf("(*Chest).Move: bad coords: (%d,%d)", x, y)
		return
	}
	chest.x = x
	chest.y = y
}

func (chest *Chest) Action(desc common.ActionDesc) {
	switch desc.Action {
	case common.A_OPEN:
		if desc.From != nil {
			desc.From.OnItemsShow(chest)
		}
	}
}

func (chest *Chest) OnItemsShow(common.ItemsHolder) {}

func (chest *Chest) Size() common.Point {
	return chest.size
}

func (chest *Chest) Volume() int {
	return chest.size.X * chest.size.Y
}

func (chest *Chest) GetAt(x, y int) common.Item {
	if x < 0 || y < 0 || x >= chest.size.X || y >= chest.size.Y {
		log.Print("(*Chest).SetAt: coords out of range")
		return common.Item{}
	}
	return chest.items[y*chest.size.X+x]
}

func (chest *Chest) SetAt(x, y int, item common.Item) {
	if x < 0 || y < 0 || x >= chest.size.X || y >= chest.size.Y {
		log.Print("(*Chest).SetAt: coords out of range")
		return
	}
	chest.items[y*chest.size.X+x] = item
	for _, listener := range chest.listeners {
		listener.OnHolderChange(chest, chest, common.Point{x, y}, item)
	}
}

// Adds one listener once
func (chest *Chest) AddListener(listener common.HolderListener) {
	//found := false
	for _, list := range chest.listeners {
		if list == listener {
			return
		}
	}
	chest.listeners = append(chest.listeners, listener)
}

func (chest *Chest) RemoveListener(listener common.HolderListener) {
	for i, hl := range chest.listeners {
		if hl == listener {
			chest.listeners = append(chest.listeners[:i], chest.listeners[i+1:]...)
			return
		}
	}
}

func (chest *Chest) ObjectID() common.ID {
	return chest.gameID
}

func (chest *Chest) AddSome() {
	mgr := common.MainHub.World.TypeManager()
	itemNum := common.RandInt(0, chest.Volume()+1)
	i := 0
	itemsMap := mgr.ItemsMap()
	totalItems := len(itemsMap)
	for i < itemNum {
		n := common.RandInt(0, chest.Volume())
		tp := common.RandInt(0, totalItems)
		id := 0
		var desc common.ItemDesc
		for _, mapDesc := range itemsMap {
			if id == tp {
				desc = *mapDesc
				break
			}
			id++
		}
		chest.items[n].Count = uint8(common.RandInt(0, int(desc.Stack)-1)) + 1
		chest.items[n].Quality = 20
		chest.items[n].ID = desc.Type
		i++
	}
}

func NewChest(gameID common.ID, size common.Point) *Chest {
	return &Chest{
		gameID: gameID,
		size:   size,
		items:  make([]common.Item, size.X*size.Y),
	}
}
