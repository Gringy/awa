package obj

import (
	"log"

	"bitbucket.org/gringy/awa/common"
)

type Tree struct {
	gameID  common.ID
	x, y    int
	current common.Chunk
}

func (tree *Tree) Tick(chunk common.Chunk) {
	//
}

func (tree *Tree) OnChangeChunk(old, new common.Chunk) {
	old.RemoveObject(tree)
	old.Del(tree)
	new.AddObject(tree)
	new.Add(tree)
	tree.current = new
}

func (tree *Tree) ID() common.ID {
	return tree.gameID
}

func (tree *Tree) Type() common.ObjectType {
	return common.OT_TREE
}

var treeDrawIndexes = []common.ObjectDrawIndex{
	common.ObjectDrawIndex{common.OT_MOB, 16},
}

func (tree *Tree) DrawIndexes() []common.ObjectDrawIndex {
	return treeDrawIndexes
}

func (tree *Tree) Speed() int {
	return 2
}

func (tree *Tree) Coord() (x, y int) {
	return tree.x, tree.y
}

func (tree *Tree) Params() common.Params {
	return common.Params{}
}

func (tree *Tree) Move(x, y int) {
	if x < 0 || y < 0 || x >= common.MAPDIM || y >= common.MAPDIM {
		log.Printf("(*Tree).Move: bad coords: (%d,%d)", x, y)
		return
	}
	tree.x = x
	tree.y = y
}

func (tree *Tree) Action(desc common.ActionDesc) {
	log.Println("(*Tree).Action: action on tree")
}

func (tree *Tree) OnItemsShow(common.ItemsHolder) {}

func NewTree(gameID common.ID) common.Object {
	return &Tree{
		gameID: gameID,
	}
}
