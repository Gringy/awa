package obj

import (
	"log"
	"math/rand"

	"bitbucket.org/gringy/awa/common"
	"bitbucket.org/gringy/awa/common/path"
)

const (
	FOLL_FIND = iota
	FOLL_FOLL
	FOLL_AWAY
)

const (
	TURNS_TO_AWAY = 10
)

type Follower struct {
	gameID common.ID
	x, y   int
	//
	turns   int
	state   int
	follID  common.ID
	awayDir common.Dir
}

func (f *Follower) Tick(chunk common.Chunk) {
	switch f.state {
	case FOLL_FIND:
		chunks := chunk.Neighbours()
		chX, chY := rand.Intn(3), rand.Intn(3)
		chunk := chunks[chY*3+chX]
		objs := chunk.Objects()
		if len(objs) != 0 {
			obj := objs[rand.Intn(len(objs))]
			f.follID = obj.ID()
			f.state = FOLL_FOLL
		}
	case FOLL_FOLL:
		chunks := chunk.Neighbours()
		fx, fy := f.Coord()
		for i, ch := range chunks {
			for _, obj := range ch.Objects() {
				if obj.ID() == f.follID {
					chX := i % 3
					chY := i / 3
					objX, objY := obj.Coord()
					dx := ((chX-1)*32 + objX) - fx
					dy := ((chY-1)*32 + objY) - fy
					if common.Abs(dx) < 3 && common.Abs(dy) < 3 {
						f.state = FOLL_AWAY
						f.turns = 0
						f.awayDir = common.RandDir()
						return
					}
					a := path.NewAstar(25, chunk, common.Point{f.x, f.y}, common.Point{dx, dy})
					a.StepN(70)
					if !a.MoveObjectTo(f, chunk, common.Point{dx, dy}) {
						goto kek
					}
					return
				}
			}
		}
	kek:
		f.turns = 0
		f.awayDir = common.RandDir()
		f.state = FOLL_AWAY
	case FOLL_AWAY:
		if f.turns < TURNS_TO_AWAY {
			var to common.Point
			li := path.NewLi(5, chunk, common.Point{f.x, f.y})
			li.MoveN(10)
			switch f.awayDir {
			case common.DIR_UP:
				to = common.Point{0, 2}
			case common.DIR_DOWN:
				to = common.Point{0, -2}
			case common.DIR_LEFT:
				to = common.Point{-2, 0}
			case common.DIR_RIGHT:
				to = common.Point{2, 0}
			}
			if !li.MoveObjectTo(f, chunk, to) {
				f.state = FOLL_FIND
			}
			f.turns++
		} else {
			f.state = FOLL_FIND
			f.turns = 0
		}
	}
}

func (f *Follower) OnChangeChunk(old, new common.Chunk) {
	old.RemoveObject(f)
	old.Del(f)
	new.AddObject(f)
	new.Add(f)
}

func (f *Follower) ID() common.ID {
	return f.gameID
}

func (f *Follower) Type() common.ObjectType {
	return common.OT_MOB
}

var followerDrawIndexes = []common.ObjectDrawIndex{
	common.ObjectDrawIndex{common.OT_MOB, 3},
}

func (f *Follower) DrawIndexes() []common.ObjectDrawIndex {
	return followerDrawIndexes
}

func (f *Follower) Speed() int {
	return 2
}

func (f *Follower) Coord() (x, y int) {
	return f.x, f.y
}

func (f *Follower) Params() common.Params {
	return common.Params{}
}

func (f *Follower) Move(x, y int) {
	if x < 0 || y < 0 || x >= common.MAPDIM || y >= common.MAPDIM {
		log.Printf("(*Follower).Move: bad coords: (%d,%d)", x, y)
		return
	}
	f.x = x
	f.y = y
}

func (f *Follower) Action(desc common.ActionDesc) {
	log.Println("(*Follower).Action: action on follower")
}

func (follower *Follower) OnItemsShow(common.ItemsHolder) {}

func NewFollower(gameID common.ID) common.Object {
	return &Follower{
		gameID: gameID,
		state:  FOLL_FIND,
	}
}
