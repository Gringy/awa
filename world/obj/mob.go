package obj

import (
	"log"

	"bitbucket.org/gringy/awa/common"
	"bitbucket.org/gringy/awa/common/path"
)

type Mob struct {
	gameID    common.ID
	x, y      int
	moveDir   common.Dir
	moveTurns int

	tick int
	tp   common.ObjectType
}

func (m *Mob) Tick(chunk common.Chunk) {
	if m.moveTurns > 6 {
		m.moveDir = common.RandDir()
		m.moveTurns = 0
	}
	li := path.NewLi(5, chunk, common.Point{m.x, m.y})
	li.MoveN(10)
	var to common.Point
	switch m.moveDir {
	case common.DIR_UP:
		to = common.Point{0, 2}
	case common.DIR_DOWN:
		to = common.Point{0, -2}
	case common.DIR_LEFT:
		to = common.Point{-2, 0}
	case common.DIR_RIGHT:
		to = common.Point{2, 0}
	}
	if !li.MoveObjectTo(m, chunk, to) {
		m.moveDir = common.RandDir()
		m.moveTurns = 0
	}
	m.moveTurns++

	m.tick++
	if m.tick%4 == 0 {
		if m.tp == common.OT_MOB {
			m.tp = common.OT_MOB_DEAD
		} else {
			m.tp = common.OT_MOB
		}
		chunk.ChangeType(m)
	}
}

func (m *Mob) OnChangeChunk(old, new common.Chunk) {
	old.RemoveObject(m)
	old.Del(m)
	new.AddObject(m)
	new.Add(m)
}

func (m *Mob) ID() common.ID {
	return m.gameID
}

func (m *Mob) Type() common.ObjectType {
	return m.tp
}

var mobDrawIndexes = []common.ObjectDrawIndex{
	common.ObjectDrawIndex{common.OT_MOB, 4},
	common.ObjectDrawIndex{common.OT_MOB_DEAD, 5},
}

func (m *Mob) DrawIndexes() []common.ObjectDrawIndex {
	return mobDrawIndexes
}

func (m *Mob) Speed() int {
	return 2
}

func (m *Mob) Coord() (x, y int) {
	return m.x, m.y
}

func (m *Mob) Params() common.Params {
	return common.Params{}
}

func (m *Mob) Move(x, y int) {
	if x < 0 || y < 0 || x >= common.MAPDIM || y >= common.MAPDIM {
		log.Printf("(*Mob).Move: bad coords: (%d,%d)", x, y)
		return
	}
	m.x = x
	m.y = y
}

func (m *Mob) Action(desc common.ActionDesc) {
	switch desc.Action {
	case common.A_KICK:
		log.Print("(*Mob).Action: A_KICK")
		desc.WhereYou.Del(m)
		desc.WhereYou.RemoveObject(m)
	}
}

func (m *Mob) OnItemsShow(common.ItemsHolder) {}

func NewMob(gameID common.ID) common.Object {
	return &Mob{
		gameID: gameID,
		tp:     common.OT_MOB,
	}
}
