package world

import (
	"log"
	"strings"

	"bitbucket.org/gringy/awa/common"
	"bitbucket.org/gringy/awa/common/msg"
	"bitbucket.org/gringy/awa/lang"
)

// Player struct is an implementation of common.Player
type Player struct {
	playerID common.PlayerID
	gameID   common.ID
	client   common.Client

	worldClient *WorldClient

	// Coordinates of Player
	x, y int
	// Params of player
	params common.Params

	runner *lang.Runner

	// HACK: need to save holder chunk in every obj and way to update it
	moveTo            common.Point
	actionRequest     ActionRequest
	ch                common.Chunk
	additionalDharmas []common.Dharma

	holders []common.ItemsHolder
}

type ActionRequest struct {
	act     common.Action
	chunkID common.ChunkID
	id      common.ID
}

func (p *Player) OnChangeChunk(old, new common.Chunk) {
	if cl := p.Client(); cl != nil {
		cl.Send(common.Message{
			Bytes: msg.ChangeMessage(old, new),
		})
	}
	old.RemoveObject(p)
	old.Del(p)
	new.AddPlayer(p)
	new.Add(p)
	common.MainHub.World.PlayerMovedTo(p.playerID, new.ChunkID())
}

func (p *Player) Tick(chunk common.Chunk) {
	if p.moveTo.X != 0 || p.moveTo.Y != 0 {
		if testPassable(p.ch, p, p.moveTo.X, p.moveTo.Y) {
			moveObject(p, p.ch, p.moveTo.X, p.moveTo.Y)
			p.moveTo = common.Point{}
		}
	}
	if p.actionRequest.act != common.A_NONE {
		log.Println("(*Player).Tick: actionRequest")
		objChunk, err := common.MainHub.World.Chunk(p.actionRequest.chunkID)
		if err != nil {
			log.Println("(*Player).Tick: ", err)
			p.actionRequest = ActionRequest{}
			return
		}
		obj, err := objChunk.GetObject(p.actionRequest.id)
		if err != nil {
			log.Println("(*Player).Tick: ", err)
			p.actionRequest = ActionRequest{}
			return
		}
		obj.Action(common.ActionDesc{WhereYou: objChunk, Action: p.actionRequest.act, From: p})
		p.actionRequest = ActionRequest{}
	}
}

func (p *Player) ExtraDharmas() []common.Dharma {
	return p.additionalDharmas
}

func (p *Player) ClearExtraDharmas() {
	p.additionalDharmas = nil
}

func (p *Player) ID() common.ID {
	return p.gameID
}

func (p *Player) Type() common.ObjectType {
	return common.OT_PLAYER
}

var playerDrawIndexes = []common.ObjectDrawIndex{
	common.ObjectDrawIndex{common.OT_PLAYER, 1},
}

func (p *Player) DrawIndexes() []common.ObjectDrawIndex {
	return playerDrawIndexes
}

func (p *Player) Speed() int {
	return 5
}

func (p *Player) Params() common.Params {
	return p.params
}

func (p *Player) OnParamsChanged() {
	p.AddExtraDharma(&msg.ParamsChangedDharma{
		Params: p.params,
	})
}

func (p *Player) SayMsg(ch common.Chunk, msg string) {
	if strings.HasPrefix(msg, "#") {
		toInterpret := strings.TrimPrefix(msg, "#")
		prog, err := lang.Parse([]byte(toInterpret))
		if err != nil {
			log.Print(err)
			return
		}
		p.runner.RunProg(prog)
		msgs := p.runner.DropMessages()
		if len(msgs) != 0 {
			for _, msg := range msgs {
				p.AddSysMsg(msg)
			}
		}
		p.AddSysMsg("ok")
		p.DropExtras()
	} else {
		ch.Say(p, msg)
	}
}

func (p *Player) AddSysMsg(message string) {
	p.AddExtraDharma(&msg.SayDharma{
		GameID: common.ID(0),
		Msg:    message,
	})
}

func (p *Player) DropExtras() {
	if p.client != nil {
		msg := msg.DeltaMessage(p.additionalDharmas)
		p.client.Send(common.Message{
			Bytes: msg,
		})
	}
	p.ClearExtraDharmas()
}

func (p *Player) ItemDescRequestMsg(ch common.Chunk, ids []uint32) {
	manager := common.MainHub.World.TypeManager()
	for _, id := range ids {
		if desc := manager.ItemById(common.ItemType(id)); desc != nil {
			p.AddExtraDharma(&msg.ItemDescDharma{
				Type:      desc.Type,
				Grp:       desc.Group,
				Name:      desc.Name,
				Stack:     desc.Stack,
				DrawIndex: desc.DrawIndex,
			})
		}
	}
	p.DropExtras()
}

func (p *Player) MoveMsg(ch common.Chunk, dx, dy int) {
	p.moveTo.X = dx
	p.moveTo.Y = dy
	p.ch = ch
}

func (p *Player) ReplaceMsg(ch common.Chunk, dx, dy int, tp uint16) {
	log.Printf("(*Player).ReplaceMsg: (%d, %d)=>%d", dx, dy, tp)
	newX, newY, chunkX, chunkY, err := common.NormalizeCoords(p.x+dx, p.y+dy)
	if err != nil {
		return
	}
	nch := ch.Neighbour(chunkX, chunkY)
	if nch == nil {
		return
	}
	cmap := nch.Map()
	cmap.SetTypeAt(newX, newY, common.BlockType(tp))
	nch.Replace(newX, newY, tp)
	log.Printf("(*Player).ReplaceMsg: cid=%d nx=%d ny=%d", nch.ChunkID(), newX, newY)
}

func (p *Player) BlockRequestMsg(ch common.Chunk, ids []uint16) {
	world := common.MainHub.World
	typeManager := world.TypeManager()
	for _, id := range ids {
		if tp := typeManager.TypeById(common.BlockType(id)); tp != nil {
			p.AddExtraDharma(&msg.BlockDescriptionDharma{
				Tp:        tp.Id,
				Passable:  tp.Walkable,
				DrawIndex: tp.DrawIndex,
				Name:      tp.Names[0],
			})
		}
	}
	p.DropExtras()
}

func (p *Player) ActionRequestMsg(ch common.Chunk, act common.Action, chunkID common.ChunkID, id common.ID) {
	p.actionRequest = ActionRequest{
		act:     act,
		chunkID: chunkID,
		id:      id,
	}
}

func (p *Player) ItemMoveMsg(ch common.Chunk, fromID common.ID, fromPoint common.Point, toID common.ID, toPoint common.Point) {
	fromHolder := p.findHolder(fromID)
	toHolder := p.findHolder(toID)
	if fromHolder == nil || toHolder == nil {
		log.Println("(*Player).ItemMoveMsg: unknown or closed holders")
		return
	}
	if err := common.ChangeItems(fromHolder, fromPoint, toHolder, toPoint); err != nil {
		log.Println("(*Player).ItemMoveMsg: ", err)
	}
}

func (p *Player) findHolder(id common.ID) common.ItemsHolder {
	for _, holder := range p.holders {
		if holder.ObjectID() == id {
			return holder
		}
	}
	return nil
}

func (p *Player) ItemActionMsg() {
	//
}

func (p *Player) Client() common.Client {
	return p.client
}

func (p *Player) SetClient(client common.Client) {
	p.client = client
}

func (p *Player) PlayerID() common.PlayerID {
	return p.playerID
}

func (p *Player) Coord() (x, y int) {
	return p.x, p.y
}

func (p *Player) Move(x, y int) {
	if x < 0 || y < 0 || x >= common.MAPDIM || y >= common.MAPDIM {
		log.Printf("(*Player).Move: bad coords: (%d,%d)", x, y)
		return
	}
	p.x = x
	p.y = y
	// deleting all holders
	for _, holder := range p.holders {
		holder.RemoveListener(p)
		p.AddExtraDharma(&msg.HolderCloseDharma{GameID: holder.ObjectID()})
	}
	p.holders = nil
}

func (player *Player) Action(desc common.ActionDesc) {
	log.Println("(*Player).Action: action on player")
}

func (player *Player) OnItemsShow(holder common.ItemsHolder) {
	player.AddExtraDharma(&msg.HolderDharma{Holder: holder, GameID: holder.ObjectID()})
	holder.AddListener(player)
	player.holders = append(player.holders, holder)
}

func (player *Player) OnHolderChange(obj common.Object, holder common.ItemsHolder, p common.Point, item common.Item) {
	player.AddExtraDharma(&msg.HolderChangeDharma{
		GameID: obj.ID(),
		Point:  p,
		Item:   item,
	})
}

func (player *Player) AddExtraDharma(dharma common.Dharma) {
	player.additionalDharmas = append(player.additionalDharmas, dharma)
}

func NewPlayer(gameID common.ID, playerID common.PlayerID) common.Player {
	playerParams := common.Params{
		STR: 10,
		DEX: 10,
		WIS: 10,
	}
	playerParams.HP = playerParams.MaxHP()
	playerParams.EP = playerParams.MaxEP()
	return &Player{
		playerID: playerID,
		gameID:   gameID,
		runner:   lang.NewEmptyRunner(),
		params:   playerParams,
	}
}
