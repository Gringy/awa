package world

import (
	"errors"
	"log"

	"bitbucket.org/gringy/awa/common"
	"bitbucket.org/gringy/awa/common/msg"
)

// Chunk struct is an implementation of common.Chunk
type Chunk struct {
	// Map of chunk
	cells common.Map

	// Objects of chunk
	objs []common.Object

	// Players of chunk
	players []common.Player

	// Neighbours of chunk include itself
	neigh [9]common.Chunk

	// Changes of chunk
	dharmas []common.Dharma

	// ID of chunk
	id common.ChunkID
}

// GetMap function returns a map of chunk
func (ch *Chunk) Map() *common.Map {
	return &ch.cells
}

// GetNeighbour function returns a neighbour of this chunk
func (ch *Chunk) Neighbour(dx, dy int) common.Chunk {
	if dx > 1 || dx < -1 || dy > 1 || dy < -1 {
		log.Panicf("(*Chunk).GetNeighbour: bad coords: (%d, %d)", dx, dy)
	}
	index := (dy+1)*3 + (dx + 1)
	return ch.neigh[index]
}

// SetNeighbour function sets chunk's neighbour
func (ch *Chunk) SetNeighbour(dx, dy int, nei common.Chunk) {
	if dx > 1 || dx < -1 || dy > 1 || dy < -1 {
		log.Panicf("(*Chunk).SetNeighbour: bad coords: (%d, %d)", dx, dy)
	}
	if dx == 0 && dy == 0 {
		log.Panic("(*Chunk).SetNeighbour: try to change itself")
	}
	index := (dy+1)*3 + (dx + 1)
	ch.neigh[index] = nei
}

func (ch *Chunk) Neighbours() []common.Chunk {
	return ch.neigh[:]
}

// AddObject function adds object to chunk
func (ch *Chunk) AddObject(obj common.Object) {
	ch.objs = append(ch.objs, obj)
}

// GetObject function finds object in chunk by id
func (ch *Chunk) GetObject(id common.ID) (common.Object, error) {
	for _, obj := range ch.objs {
		if obj.ID() == id {
			return obj, nil
		}
	}
	return nil, errors.New("(*Chunk).GetObject: no such object")
}

func (ch *Chunk) RemoveObject(obj common.Object) {
	for i, chObj := range ch.objs {
		if chObj.ID() == obj.ID() {
			ch.objs = append(ch.objs[:i], ch.objs[i+1:]...)
			return
		}
	}
	for i, chObj := range ch.players {
		if chObj.ID() == obj.ID() {
			ch.players = append(ch.players[:i], ch.players[i+1:]...)
			return
		}
	}
}

// AddPlayer function adds player to chunk
func (ch *Chunk) AddPlayer(player common.Player) {
	ch.players = append(ch.players, player)
}

// GetPlayer function finds player in chunk by id
func (ch *Chunk) GetPlayer(playerID common.PlayerID) (common.Player, error) {
	for _, player := range ch.players {
		if player.PlayerID() == playerID {
			return player, nil
		}
	}
	return nil, errors.New("(*Chunk).GetPlayer: no such player")
}

func (ch *Chunk) Objects() []common.Object {
	objects := make([]common.Object, len(ch.objs)+len(ch.players))
	i := 0
	for _, obj := range ch.objs {
		objects[i] = obj
		i++
	}
	for _, player := range ch.players {
		objects[i] = player
		i++
	}
	return objects
}

// ID function
func (ch *Chunk) ChunkID() common.ChunkID {
	return ch.id
}

// Tick function do the tick of chunk
func (ch *Chunk) Tick() {
	for _, mob := range ch.objs {
		mob.Tick(ch)
	}
	for _, player := range ch.players {
		player.Tick(ch)
	}
}

// SendDelta function
func (ch *Chunk) SendDelta() {
	if len(ch.players) == 0 {
		return
	}
	chunkDharmas := ch.getDharmas()
	for _, player := range ch.players {
		if player.Client() != nil {
			msg := msg.DeltaMessageFromTwo(chunkDharmas, player.ExtraDharmas())
			player.Client().Send(common.Message{
				Bytes: msg,
			})
			player.ClearExtraDharmas()
		}
	}
}

func (ch *Chunk) getDharmas() []common.Dharma {
	dharmas := ch.dharmas
	for _, chunk := range ch.neigh {
		if chunk != nil && chunk != ch {
			dharmas = append(dharmas, chunk.Dharmas()...)
		}
	}

	return dharmas
}

func (ch *Chunk) Dharmas() []common.Dharma {
	return ch.dharmas
}

// ClearDelta function
func (ch *Chunk) ClearDelta() {
	ch.dharmas = nil
}

// Dharma functions

func (ch *Chunk) Move(obj common.Object) {
	x, y := obj.Coord()
	ch.dharmas = append(ch.dharmas, &msg.MoveDharma{
		GameID: obj.ID(),
		X:      x,
		Y:      y,
	})
}

func (ch *Chunk) Add(obj common.Object) {
	ch.dharmas = append(ch.dharmas, &msg.AddDharma{
		ChunkID: ch.id,
		Obj:     obj,
	})
}

func (ch *Chunk) Del(obj common.Object) {
	ch.dharmas = append(ch.dharmas, &msg.DelDharma{
		ChunkID: ch.id,
		GameID:  obj.ID(),
	})
}

func (ch *Chunk) Say(obj common.Object, message string) {
	ch.dharmas = append(ch.dharmas, &msg.SayDharma{
		GameID: obj.ID(),
		Msg:    message,
	})
}

func (ch *Chunk) Replace(x, y int, tp uint16) {
	ch.dharmas = append(ch.dharmas, &msg.ReplaceDharma{
		ChunkID: ch.ChunkID(),
		X:       byte(x),
		Y:       byte(y),
		Tp:      tp,
	})
}

func (ch *Chunk) ChangeType(obj common.Object) {
	ch.dharmas = append(ch.dharmas, &msg.ObjectTypeChangeDharma{
		Id:   obj.ID(),
		Type: obj.Type(),
	})
}

// NewChunk function returns new empty chunk
func NewChunk(id common.ChunkID) common.Chunk {
	chunk := new(Chunk)

	chunk.id = id
	chunk.neigh[4] = chunk

	return chunk
}
