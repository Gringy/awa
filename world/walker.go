package world

import "bitbucket.org/gringy/awa/common"

type walker struct {
	current common.Chunk
	place   common.Point
	mgr     common.TypeManager
}

func (w *walker) getAt(dx, dy int) common.Block {
	newX, newY, chX, chY, err := common.NormalizeCoords(w.place.X+dx, w.place.Y+dy)
	if err != nil {
		return common.Block{}
	}
	return w.current.Neighbour(chX, chY).Map().GetAt(newX, newY)
}

func (w *walker) setAt(dx, dy int, tp common.Block) {
	newX, newY, chX, chY, err := common.NormalizeCoords(w.place.X+dx, w.place.Y+dy)
	if err != nil {
		return
	}
	w.current.Neighbour(chX, chY).Map().SetAt(newX, newY, tp)
}

func (w *walker) setTypeAt(dx, dy int, tp common.BlockType) {
	newX, newY, chX, chY, err := common.NormalizeCoords(w.place.X+dx, w.place.Y+dy)
	if err != nil {
		return
	}
	w.current.Neighbour(chX, chY).Map().SetTypeAt(newX, newY, tp)
}

func (w *walker) setTypeInLine(dir common.Dir, tp common.BlockType, n int) {
	var isX, isY int
	switch dir {
	case common.DIR_LEFT, common.DIR_RIGHT:
		isY = 1
	case common.DIR_DOWN, common.DIR_UP:
		isX = 1
	}
	w.setTypeAt(n*isX, n*isY, tp)
	// if w.getAt(0, 0).IsEmpty() &&
	// 	w.getAt(isX, isY).IsEmpty() &&
	// 	w.getAt(-isX, -isY).IsEmpty() {
	// 	return false
	// }
	// return true
}

func (w *walker) moveTo(dx, dy int) {
	newX, newY, chX, chY, err := common.NormalizeCoords(w.place.X+dx, w.place.Y+dy)
	if err != nil {
		return
	}
	if chX != 0 || chY != 0 {
		w.current = w.current.Neighbour(chX, chY)
	}
	w.place.X = newX
	w.place.Y = newY
}

func (w *walker) moveDir(dir common.Dir) {
	switch dir {
	case common.DIR_DOWN:
		w.moveTo(0, -1)
	case common.DIR_LEFT:
		w.moveTo(-1, 0)
	case common.DIR_RIGHT:
		w.moveTo(1, 0)
	case common.DIR_UP:
		w.moveTo(0, 1)
	}
}

func (w *walker) fork() *walker {
	newWalker := *w
	return &newWalker
}

func (w *walker) isSquareEmpty(size int) bool {
	for x := -size; x <= size; x++ {
		for y := -size; y <= size; y++ {
			if !w.empty(x, y) {
				return false
			}
		}
	}
	return true
}

func (w *walker) moveBack(dir common.Dir) {
	switch dir {
	case common.DIR_UP:
		w.moveTo(0, -1)
	case common.DIR_RIGHT:
		w.moveTo(-1, 0)
	case common.DIR_LEFT:
		w.moveTo(1, 0)
	case common.DIR_DOWN:
		w.moveTo(0, 1)
	}
}

func (w *walker) empty(dx, dy int) bool {
	return w.getAt(dx, dy).IsEmpty()
}

func (w *walker) isHere(dir common.Dir) bool {
	var isX, isY int
	switch dir {
	case common.DIR_LEFT, common.DIR_RIGHT:
		isY = 1
	case common.DIR_DOWN, common.DIR_UP:
		isX = 1
	}
	if w.getAt(0, 0).IsEmpty() &&
		w.getAt(isX, isY).IsEmpty() &&
		w.getAt(-isX, -isY).IsEmpty() {
		return false
	}
	return true
}

func (w *walker) isWall(tp common.Block) bool {
	return !w.mgr.TypeById(tp.Type).Walkable
}

func (w *walker) isFloor(tp common.Block) bool {
	return tp != common.Block{} && w.mgr.TypeById(tp.Type).Walkable
}

func (w *walker) isHereWall(dir common.Dir) bool {
	var isX, isY int
	switch dir {
	case common.DIR_LEFT, common.DIR_RIGHT:
		isY = 1
	case common.DIR_DOWN, common.DIR_UP:
		isX = 1
	}
	if w.isWall(w.getAt(0, 0)) &&
		w.isWall(w.getAt(isX, isY)) &&
		w.isWall(w.getAt(-isX, -isY)) {
		return true
	}
	return false
}

func (w *walker) isHereFloor(dir common.Dir) bool {
	var isX, isY int
	switch dir {
	case common.DIR_LEFT, common.DIR_RIGHT:
		isY = 1
	case common.DIR_DOWN, common.DIR_UP:
		isX = 1
	}
	if w.isFloor(w.getAt(0, 0)) &&
		w.isFloor(w.getAt(isX, isY)) &&
		w.isFloor(w.getAt(-isX, -isY)) {
		return true
	}
	return false
}
