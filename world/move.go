package world

import (
	"log"

	"bitbucket.org/gringy/awa/common"
	"bitbucket.org/gringy/awa/common/path"
)

func testPassable(ch common.Chunk, o common.Object, dx, dy int) bool {
	ox, oy := o.Coord()
	speed := o.Speed()
	li := path.NewLi(speed, ch, common.Point{ox, oy})
	for i := 0; i < speed*2; i++ {
		li.Move()
	}
	cell := li.GetAt(dx, dy)
	if cell != path.WALL && cell != path.UNCHECKED && cell <= speed {
		return true
	}
	return false
}

func passableBy(ch common.Chunk, o common.Object, dx, dy, size int) int {
	ox, oy := o.Coord()
	li := path.NewLi(size, ch, common.Point{ox, oy})
	for i := 0; i < size*3; i++ {
		li.Move()
	}
	return li.GetAt(dx, dy)
}

func moveObject(o common.Object, ch common.Chunk, dx, dy int) {
	px, py := o.Coord()

	newX, newY, chX, chY, err := common.NormalizeCoords(px+dx, py+dy)
	if err != nil {
		log.Print("moveObject: ", err.Error())
		return
	}
	// move inside chunk
	if chX == 0 && chY == 0 {
		o.Move(newX, newY)
		ch.Move(o)
		return
	}
	// move between chunks
	newChunk := ch.Neighbour(chX, chY)
	o.Move(newX, newY)
	o.OnChangeChunk(ch, newChunk)
}
